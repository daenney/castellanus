package main

import (
	"fmt"

	"github.com/alexflint/go-arg"

	"gitlab.gnome.org/daenney/castellanus/internal/cmd/generate"
	"gitlab.gnome.org/daenney/castellanus/internal/cmd/validate"
	"gitlab.gnome.org/daenney/castellanus/internal/cmd/validatemr"
)

var (
	version   = "unknown"
	commit    = "unknown"
	buildDate = "unknown"
)

type root struct {
	Generate   *generate.Cmd   `arg:"subcommand:generate" help:"generate a location or city from an OSM URL"`
	Validate   *validate.Cmd   `arg:"subcommand:validate" help:"validate Locations.xml changes by comparing two file versions"`
	ValidateMR *validatemr.Cmd `arg:"subcommand:validate-mr" help:"validate Locations.xml changes proposed in a Gitlab MR"`
}

func (root) Version() string {
	return fmt.Sprintf("Version: %s\nCommit: %s\nBuild: %s\n", version, commit, buildDate)
}

func (root) Description() string {
	return "Castellanus assists with validating changes to libgweather's " +
		"Locations.xml. It may also be used to generate new location or city " +
		"entries that can be submitted to libgweather for inclusion.\n\n" +
		"[GENERATE]\n\n" +
		generate.Description + "\n" +
		"[VALIDATE]\n\n" +
		validate.Description + "\n" +
		"[VALIDATE-MR]\n\n" +
		validatemr.Description
}

func (root) Epilogue() string {
	return "This tools makes use of data provided by OpenStreetMap and the " +
		"NOAA National Weather Service."
}

func main() {
	app := root{}
	arg.MustParse(&app)

	switch {
	case app.Validate != nil:
		app.Validate.UAVersion = version
		app.Validate.Run()
	case app.ValidateMR != nil:
		app.ValidateMR.UAVersion = version
		app.ValidateMR.Run()
	case app.Generate != nil:
		app.Generate.UAVersion = version
		app.Generate.Run()
	}
}
