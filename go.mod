module gitlab.gnome.org/daenney/castellanus

go 1.19

require (
	github.com/alexflint/go-arg v1.4.3
	github.com/elliotchance/orderedmap/v2 v2.2.0
	github.com/jftuga/geodist v1.0.0
	github.com/mitchellh/hashstructure/v2 v2.0.2
	golang.org/x/time v0.0.0-20220922220347-f3bd1da661af
)

require (
	github.com/alexflint/go-scalar v1.2.0 // indirect
	github.com/stretchr/testify v1.8.0 // indirect
)
