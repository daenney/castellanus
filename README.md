# Castellanus

This CLI aims to assist maintainers of [libgweather][lgw] in handling location
data requests and validating PRs updating location info.

[OpenStreetMap (OSM) Nominatim][osmn] and [NOAA National Weather Service][noaa]
are used by this tool to validate the proposed changed.

## Usage

Castellanus provides a couple of features:
* Validation of updates to `Locations.xml` by comparing a previous and new state
* Validation of an MR updating `Locations.xml`
* Generation of a `<location>` or `<city>` based on an OSM link

For an overview of the commands and their options, use `--help`.

### Validation

The validation rules castellanus implements vary depending on whether we're
validation a `location` or a `city`.

For `location`:
* METAR data is available
* METAR station coordinates roughly matches the location coordinates

For `city`:
* Name matches OSM
* City coordinates roughly matches OSM
* City population meets the libgweather inclusion threshold

### Generation

The `generate` subcommand takes one or multiple OSM URLs and returns a `<city>`
or `<location>` XML stanza. The OSM URL needs to be of the form
`https://www.openstreetmap.org/<type>/<ID>` though the shorthands of
`/<type>/<id>` and `<type>/<id>` are also understood.

## Build

This tool is built in [Go][go] and can be built with a module-enabled version
of the Go toolchain. `go build` is all you need, there are no C-based
dependencies.

[lgw]: https://gitlab.gnome.org/GNOME/libgweather
[osmn]: https://nominatim.org/release-docs/develop/api/Overview/
[noaa]: https://www.aviationweather.gov/dataserver
[go]: https://go.dev/
