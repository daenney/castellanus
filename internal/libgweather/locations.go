package libgweather

import (
	"encoding/xml"
	"fmt"
	"strings"

	"github.com/elliotchance/orderedmap/v2"
	"github.com/mitchellh/hashstructure/v2"
	"gitlab.gnome.org/daenney/castellanus/internal/geo"
)

type GWeather struct {
	XMLName       xml.Name      `xml:"gweather"`
	Format        string        `xml:"format,attr"`
	NamedTimezone NamedTimezone `xml:"named-timezone"`
	Regions       []Region      `xml:"region"`
}

func (g GWeather) ToEntries() (*Entries, error) {
	res := orderedmap.NewOrderedMap[uint64, Entry]()
	for _, r := range g.Regions {
		r := r
		for _, c := range r.Countries {
			c := c
			for _, s := range c.States {
				s := s
				for _, loc := range s.Locations {
					loc := loc
					e := locationToEntry(r.Name, c.Name, c.CodeISO, &s.Name, loc)
					hash, err := hashstructure.Hash(e, hashstructure.FormatV2, nil)
					if err != nil {
						return nil, err
					}
					res.Set(hash, e)
				}
				for _, cit := range s.Cities {
					cit := cit
					e := cityToEntry(r.Name, c.Name, c.CodeISO, &s.Name, cit)
					hash, err := hashstructure.Hash(e, hashstructure.FormatV2, nil)
					if err != nil {
						return nil, err
					}
					res.Set(hash, e)
				}
			}
			for _, loc := range c.Locations {
				loc := loc
				e := locationToEntry(r.Name, c.Name, c.CodeISO, nil, loc)
				hash, err := hashstructure.Hash(e, hashstructure.FormatV2, nil)
				if err != nil {
					return nil, err
				}
				res.Set(hash, e)
			}
			for _, cit := range c.Cities {
				cit := cit
				e := cityToEntry(r.Name, c.Name, c.CodeISO, nil, cit)
				hash, err := hashstructure.Hash(e, hashstructure.FormatV2, nil)
				if err != nil {
					return nil, err
				}
				res.Set(hash, e)
			}
		}
	}
	return res, nil
}

func locationToEntry(region, country, code string, state *string, loc Location) Entry {
	return Entry{
		Region:      region,
		Country:     country,
		CountryCode: code,
		State:       state,
		Name:        loc.Name,
		METAR:       &loc.CodeMETAR,
		Coordinates: loc.Coordinates,
	}
}

func cityToEntry(region, country, code string, state *string, city City) Entry {
	return Entry{
		Region:      region,
		Country:     country,
		CountryCode: code,
		Name:        city.Name,
		State:       state,
		Coordinates: city.Coordinates,
		Population:  city.Population,
	}
}

// Timezone is an area which observes a uniform standard time for legal,
// commercial and social purposes
type Timezone struct {
	XMLNname  xml.Name `xml:"timezone"`
	Name      *string  `xml:"_name"`
	ID        string   `xml:"id,attr"`
	Obsoletes []string `xml:"obsoletes,omitempty"`
}

// NamedTimezone is a special grouping of some timezones
type NamedTimezone struct {
	Name         string     `xml:"_name"`
	Code         string     `xml:"code"`
	Timezones    []Timezone `xml:"timezones>timezone"`
	TimezoneHint string     `xml:"tz-hint"`
}

// Region is a region in the world, like Africa, but not always the same as a continent
type Region struct {
	XMLName   xml.Name  `xml:"region"`
	Name      string    `xml:"_name"`
	Countries []Country `xml:"country"`
}

// Country is an internationally recognised sovereign state in a Region
type Country struct {
	XMLName           xml.Name   `xml:"country"`
	Name              string     `xml:"_name"`
	CodeISO           string     `xml:"iso-code"`
	CodeFIPS          []string   `xml:"fips-code"`
	PreferredLanguage *string    `xml:"pref-lang"`
	Timezones         []Timezone `xml:"timezones>timezone"`
	TimezoneHint      *string    `xml:"tz-hint"`
	Locations         []Location `xml:"location,omitempty"`
	Cities            []City     `xml:"city,omitempty"`
	States            []State    `xml:"state,omitempty"`
}

// Location represents a location for which weather data can be retrieved
type Location struct {
	XMLName     xml.Name         `xml:"location"`
	Name        string           `xml:"name"`
	CodeMETAR   string           `xml:"code"`
	Coordinates *geo.Coordinates `xml:"coordinates"`
	Radar       *string          `xml:"radar"`
	Zone        *string          `xml:"zone"`
}

// City is an urban population center in a country
type City struct {
	XMLName      xml.Name         `xml:"city"`
	Name         string           `xml:"_name"`
	Coordinates  *geo.Coordinates `xml:"coordinates"`
	TimezoneHint *string          `xml:"tz-hint"`
	Population   *uint64          `xml:"population"`
}

// State is a subdivision of a country
type State struct {
	XMLName      xml.Name   `xml:"state"`
	Name         string     `xml:"_name"`
	CodeFIPS     []string   `xml:"fips-code"`
	TimezoneHint *string    `xml:"tz-hint"`
	Locations    []Location `xml:"location,omitempty"`
	Cities       []City     `xml:"city,omitempty"`
}

type Entries = orderedmap.OrderedMap[uint64, Entry]

type Entry struct {
	Region, Country, Name string
	CountryCode           string
	State                 *string
	METAR                 *string
	Coordinates           *geo.Coordinates
	Population            *uint64
}

func (e Entry) String() string {
	s := strings.Builder{}
	s.WriteString(e.Name)
	if e.METAR != nil {
		s.WriteString(fmt.Sprintf(" (%s)", *e.METAR))
	}
	s.WriteString(", ")
	if e.State != nil {
		s.WriteString(*e.State)
		s.WriteString(", ")
	}
	s.WriteString(e.Country)
	if e.Coordinates != nil {
		s.WriteString(fmt.Sprintf(" (lat: %.6f, lon: %.6f)", e.Coordinates.Coord.Lat, e.Coordinates.Coord.Lon))
	}

	return s.String()
}

// Changed returns Entries from the new set that aren't found in the old set
//
// This will return both added entries as well as modified entries because the
// hash of the Entry will have changed.
func Changed(old, new *Entries) *Entries {
	if old == nil && new == nil {
		return nil
	}
	if old != nil && new == nil {
		return nil
	}
	if old == nil && new != nil && new.Len() == 0 {
		return nil
	}
	if old != nil && new != nil && old.Len() == 0 && new.Len() == 0 {
		return nil
	}
	if old == nil && new != nil {
		return new
	}

	res := orderedmap.NewOrderedMap[uint64, Entry]()

	for el := new.Front(); el != nil; el = el.Next() {
		v := old.GetElement(el.Key)
		if v == nil {
			res.Set(el.Key, el.Value)
		}
	}

	if res.Len() == 0 {
		return nil
	}
	return res
}
