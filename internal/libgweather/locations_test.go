package libgweather

import (
	"reflect"
	"testing"

	"github.com/elliotchance/orderedmap/v2"
	"github.com/mitchellh/hashstructure/v2"
	"gitlab.gnome.org/daenney/castellanus/internal/geo"
)

func ptr[T any](t *testing.T, x T) *T {
	t.Helper()
	return &x
}

func TestEntryToString(t *testing.T) {
	tests := []struct {
		name string
		in   Entry
		out  string
	}{
		{
			name: "location without state",
			in:   Entry{Name: "Athens International Airport Eleftherios Venizelos", Region: "Europe", Country: "Greece", CountryCode: "GR", METAR: ptr(t, "LGAV"), Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
			out:  "Athens International Airport Eleftherios Venizelos (LGAV), Greece (lat: 1.000000, lon: 1.000000)",
		},
		{
			name: "location with state",
			in:   Entry{Name: "Athens International Airport Eleftherios Venizelos", Region: "Europe", Country: "Greece", CountryCode: "GR", State: ptr(t, "Attica"), METAR: ptr(t, "LGAV"), Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
			out:  "Athens International Airport Eleftherios Venizelos (LGAV), Attica, Greece (lat: 1.000000, lon: 1.000000)",
		},
		{
			name: "city without state",
			in:   Entry{Name: "Athens", Region: "Europe", Country: "Greece", CountryCode: "GR", Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
			out:  "Athens, Greece (lat: 1.000000, lon: 1.000000)",
		},
		{
			name: "city with state",
			in:   Entry{Name: "Athens", Region: "Europe", Country: "Greece", CountryCode: "GR", State: ptr(t, "Attica"), Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
			out:  "Athens, Attica, Greece (lat: 1.000000, lon: 1.000000)",
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res := tt.in.String()
			if res != tt.out {
				t.Fatalf("Expected string: %s, got: %s", tt.out, res)
			}
		})
	}
}

func TestCityToEntry(t *testing.T) {
	tests := []struct {
		name, region, country, code string
		state                       *string
		in                          City
		out                         Entry
	}{
		{
			name:    "city without state",
			region:  "Europe",
			country: "Greece",
			code:    "GR",
			state:   nil,
			in:      City{Name: "Athens", Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
			out:     Entry{Name: "Athens", Region: "Europe", Country: "Greece", CountryCode: "GR", Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
		},
		{
			name:    "city with state",
			region:  "Europe",
			country: "Greece",
			code:    "GR",
			state:   ptr(t, "Attica"),
			in:      City{Name: "Athens", Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
			out:     Entry{Name: "Athens", Region: "Europe", Country: "Greece", State: ptr(t, "Attica"), CountryCode: "GR", Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res := cityToEntry(tt.region, tt.country, tt.code, tt.state, tt.in)
			if !reflect.DeepEqual(res, tt.out) {
				t.Fatalf("Expected equality")
			}
		})
	}
}

func TestLocationToEntry(t *testing.T) {
	tests := []struct {
		name, region, country, code string
		state                       *string
		in                          Location
		out                         Entry
	}{
		{
			name:    "location without state",
			region:  "Europe",
			country: "Greece",
			code:    "GR",
			state:   nil,
			in:      Location{Name: "Athens International Airport Eleftherios Venizelos", CodeMETAR: "LGAV", Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
			out:     Entry{Name: "Athens International Airport Eleftherios Venizelos", Region: "Europe", Country: "Greece", METAR: ptr(t, "LGAV"), CountryCode: "GR", Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
		},
		{
			name:    "location with state",
			region:  "Europe",
			country: "Greece",
			code:    "GR",
			state:   ptr(t, "Attica"),
			in:      Location{Name: "Athens International Airport Eleftherios Venizelos", CodeMETAR: "LGAV", Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
			out:     Entry{Name: "Athens International Airport Eleftherios Venizelos", Region: "Europe", Country: "Greece", METAR: ptr(t, "LGAV"), State: ptr(t, "Attica"), CountryCode: "GR", Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res := locationToEntry(tt.region, tt.country, tt.code, tt.state, tt.in)
			if !reflect.DeepEqual(res, tt.out) {
				t.Fatalf("Expected equality")
			}
		})
	}
}

func orderedMapWithEntries(t *testing.T, entries ...Entry) *Entries {
	t.Helper()
	res := orderedmap.NewOrderedMap[uint64, Entry]()
	for _, e := range entries {
		e := e
		hash, err := hashstructure.Hash(e, hashstructure.FormatV2, nil)
		if err != nil {
			t.FailNow()
		}
		res.Set(hash, e)
	}
	return res
}

func TestGWeatherToEntries(t *testing.T) {
	tests := []struct {
		name string
		in   GWeather
		out  *Entries
		err  bool
	}{
		{
			name: "empty",
			in:   GWeather{},
			out:  &orderedmap.OrderedMap[uint64, Entry]{},
		},
		{
			name: "region no country",
			in: GWeather{Regions: []Region{
				{Name: "Europe"},
			}},
			out: &orderedmap.OrderedMap[uint64, Entry]{},
		},
		{
			name: "region country no state location city",
			in: GWeather{Regions: []Region{
				{Name: "Europe", Countries: []Country{
					{Name: "Greece"},
				}},
			}},
			out: &orderedmap.OrderedMap[uint64, Entry]{},
		},
		{
			name: "region with country location and city",
			in: GWeather{
				Regions: []Region{
					{
						Name: "Europe",
						Countries: []Country{
							{
								Name:    "Greece",
								CodeISO: "GR",
								Locations: []Location{
									{
										Name:        "Athens International Airport Eleftherios Venizelos",
										CodeMETAR:   "LGAV",
										Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0)),
									},
								},
								Cities: []City{
									{
										Name:        "Athens",
										Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0)),
									},
								},
							},
						},
					},
				},
			},
			out: orderedMapWithEntries(t, Entry{Name: "Athens International Airport Eleftherios Venizelos"}, Entry{Name: "Athens"}),
		},
		{
			name: "region with country state location and city",
			in: GWeather{
				Regions: []Region{
					{
						Name: "Europe",
						Countries: []Country{
							{
								Name:    "Greece",
								CodeISO: "GR",
								States: []State{
									{
										Name: "Attica",
										Locations: []Location{
											{
												Name:        "Athens International Airport Eleftherios Venizelos",
												CodeMETAR:   "LGAV",
												Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0)),
											},
										},
										Cities: []City{
											{
												Name:        "Athens",
												Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0)),
											},
										},
									},
								},
							},
						},
					},
				},
			},
			out: orderedMapWithEntries(t, Entry{Name: "Athens International Airport Eleftherios Venizelos"}, Entry{Name: "Athens"}),
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res, err := tt.in.ToEntries()
			if err != nil && !tt.err {
				t.Fatalf("Did not expect an error, got: %v", err)
			}
			if err == nil && tt.err {
				t.Fatalf("Expected an error, got nil")
			}
			if err == nil {
				if res.Len() != tt.out.Len() {
					t.Fatalf("Expected: %d entries, got: %d", tt.out.Len(), res.Len())
				}
			}
		})
	}
}

func TestChanged(t *testing.T) {
	tests := []struct {
		name          string
		old, new, out *Entries
	}{
		{
			name: "nil old and new",
			old:  nil,
			new:  nil,
			out:  nil,
		},
		{
			name: "nil old and empty new",
			old:  nil,
			new:  orderedmap.NewOrderedMap[uint64, Entry](),
			out:  nil,
		},
		{
			name: "empty old and nil new",
			old:  orderedmap.NewOrderedMap[uint64, Entry](),
			new:  nil,
			out:  nil,
		},
		{
			name: "empty",
			old:  orderedmap.NewOrderedMap[uint64, Entry](),
			new:  orderedmap.NewOrderedMap[uint64, Entry](),
			out:  nil,
		},
		{
			name: "nil old one new entry",
			old:  nil,
			new:  orderedMapWithEntries(t, Entry{}),
			out:  orderedMapWithEntries(t, Entry{}),
		},
		{
			name: "updated entry name",
			old:  orderedMapWithEntries(t, Entry{Name: "Athens International Airport Eleftherios Venizelos"}),
			new:  orderedMapWithEntries(t, Entry{Name: "Διεθνής Αερολιμένας Αθηνών «Ελευθέριος Βενιζέλος»"}),
			out:  orderedMapWithEntries(t, Entry{Name: "Διεθνής Αερολιμένας Αθηνών «Ελευθέριος Βενιζέλος»"}),
		},
		{
			name: "updated entry region",
			old:  orderedMapWithEntries(t, Entry{Region: "Europe"}),
			new:  orderedMapWithEntries(t, Entry{Region: "EMEA"}),
			out:  orderedMapWithEntries(t, Entry{Region: "EMEA"}),
		},
		{
			name: "updated entry country",
			old:  orderedMapWithEntries(t, Entry{Country: "Greece"}),
			new:  orderedMapWithEntries(t, Entry{Country: "Ελληνική Δημοκρατία"}),
			out:  orderedMapWithEntries(t, Entry{Country: "Ελληνική Δημοκρατία"}),
		},
		{
			name: "updated entry state",
			old:  orderedMapWithEntries(t, Entry{}),
			new:  orderedMapWithEntries(t, Entry{State: ptr(t, "Attica")}),
			out:  orderedMapWithEntries(t, Entry{State: ptr(t, "Attica")}),
		},
		{
			name: "updated entry country code",
			old:  orderedMapWithEntries(t, Entry{CountryCode: "GR"}),
			new:  orderedMapWithEntries(t, Entry{CountryCode: "HEL"}),
			out:  orderedMapWithEntries(t, Entry{CountryCode: "HEL"}),
		},
		{
			name: "updated entry METAR",
			old:  orderedMapWithEntries(t, Entry{METAR: ptr(t, "LGAV")}),
			new:  orderedMapWithEntries(t, Entry{METAR: ptr(t, "VAGL")}),
			out:  orderedMapWithEntries(t, Entry{METAR: ptr(t, "VAGL")}),
		},
		{
			name: "updated entry coordinates",
			old:  orderedMapWithEntries(t, Entry{}),
			new:  orderedMapWithEntries(t, Entry{Coordinates: ptr(t, geo.NewCoordinates(2.0, 1.0))}),
			out:  orderedMapWithEntries(t, Entry{Coordinates: ptr(t, geo.NewCoordinates(2.0, 1.0))}),
		},
		{
			name: "updated entry population",
			old:  orderedMapWithEntries(t, Entry{}),
			new:  orderedMapWithEntries(t, Entry{Population: ptr[uint64](t, 100)}),
			out:  orderedMapWithEntries(t, Entry{Population: ptr[uint64](t, 100)}),
		},
		{
			name: "additional entry",
			old:  orderedMapWithEntries(t, Entry{Region: "Europe", Country: "Greece", CountryCode: "GR", Name: "Athens", Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))}),
			new: orderedMapWithEntries(
				t,
				Entry{Region: "Europe", Country: "Greece", CountryCode: "GR", Name: "Athens", Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
				Entry{Region: "Europe", Country: "Belgium", CountryCode: "BE", Name: "Brussels", Coordinates: ptr(t, geo.NewCoordinates(2.0, 2.0))},
			),
			out: orderedMapWithEntries(t, Entry{Region: "Europe", Country: "Belgium", CountryCode: "BE", Name: "Brussels", Coordinates: ptr(t, geo.NewCoordinates(2.0, 2.0))}),
		},
		{
			name: "remove entry",
			old: orderedMapWithEntries(
				t,
				Entry{Region: "Europe", Country: "Greece", CountryCode: "GR", Name: "Athens", Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
				Entry{Region: "Europe", Country: "Belgium", CountryCode: "BE", Name: "Brussels", Coordinates: ptr(t, geo.NewCoordinates(2.0, 2.0))},
			),
			new: orderedMapWithEntries(t, Entry{Region: "Europe", Country: "Greece", CountryCode: "GR", Name: "Athens", Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))}),
			out: nil,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res := Changed(tt.old, tt.new)
			if tt.out == nil && res != nil {
				t.Fatalf("Expected nil, got: %#v", res)
			}
			if tt.out != nil && res == nil {
				t.Fatalf("Expected: %#v, got: nil", tt.out)
			}
			if tt.out != nil && res != nil {
				rk := res.Keys()
				ok := tt.out.Keys()
				if len(rk) != len(ok) {
					t.Fatalf("Expected: %d entries, got: %d", len(ok), len(rk))
				}
				if !reflect.DeepEqual(rk, ok) {
					t.Fatalf("Expected the same set of keys")
				}
			}
		})
	}
}
