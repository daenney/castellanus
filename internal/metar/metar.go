package metar

import (
	"bufio"
	"fmt"
	"io"
	"strings"

	"gitlab.gnome.org/daenney/castellanus/internal/geo"
)

const (
	lineLength      = 83
	stationsMapSize = 8000 // There's a little over 8700 METAR stations
)

type Stations map[string]Station

type Station struct {
	State       string
	Name        string
	ICAO        string
	Coordinates *geo.Coordinates
	Country     string
}

// MustParse does the same thing as Parse, but will panic if Parse returned
// any errors
func MustParse(data io.Reader) Stations {
	stations, errs := Parse(data)
	if len(errs) != 0 {
		panic("Stations should never fail to parse")
	}
	return stations
}

// Parse returns all stations with an ICAO code for which METAR data is
// available
func Parse(data io.Reader) (Stations, []error) {
	stations := make(Stations, stationsMapSize)
	errors := []error{}

	scanner := bufio.NewScanner(data)
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) != lineLength {
			continue
		}
		if strings.HasPrefix(line, "!") {
			continue
		}

		metar := line[62:63]
		// X signifies METAR data is available and we don't care about any
		// aerodrome that doesn't publish METAR data
		if metar != "X" {
			continue
		}

		icao := strings.TrimSpace(line[20:24])
		// anything without an ICAO code isn't something that can be used
		// in libgweather, so we can pretend it doesn't exist
		if icao == "" {
			continue
		}

		s := Station{
			State:   strings.TrimSpace(line[:2]),
			Name:    strings.TrimRight(line[3:19], " "),
			ICAO:    icao,
			Country: line[81:lineLength],
		}

		lat := strings.TrimLeft(line[39:45], " ")
		lon := strings.TrimLeft(line[47:54], " ")

		coords, err := geo.CoordinatesFromDegrees(lat, lon)
		if err == nil {
			s.Coordinates = &coords
		} else {
			errors = append(errors, fmt.Errorf("failed to parse coordinates for %s: %w", s.ICAO, err))
		}

		if v, ok := stations[s.ICAO]; !ok {
			stations[s.ICAO] = s
		} else {
			// KHQG shows up twice, once with AWOS and once without, but it is
			// otherwise identical
			if s.ICAO != "KHQG" {
				errors = append(errors, fmt.Errorf("duplicate entry: %+v and %+v", v, s))
			}
		}
	}

	return stations, errors
}
