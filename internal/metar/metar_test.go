package metar

import (
	"bytes"
	"io"
	"testing"

	"gitlab.gnome.org/daenney/castellanus/internal/geo"
)

func ptr[T any](t *testing.T, x T) *T {
	t.Helper()
	return &x
}

func TestPars(t *testing.T) {
	tests := []struct {
		name   string
		in     io.Reader
		out    map[string]Station
		errors int
	}{
		{
			name: "valid with METAR",
			in:   bytes.NewBufferString("AK ADAK NAS         PADK  ADK   70454  51 53N  176 39W    3   X     T          7 US"),
			out: map[string]Station{
				"PADK": {State: "AK", Name: "ADAK NAS", ICAO: "PADK", Country: "US", Coordinates: ptr(t, geo.NewCoordinates(51.883333, -176.650000))},
			},
		},
		{
			name: "valid but no METAR",
			in:   bytes.NewBufferString("AK ADAK NAS         PADK  ADK   70454  51 53N  176 39W    3         T          7 US"),
			out:  map[string]Station{},
		},
		{
			name: "valid but no ICAO",
			in:   bytes.NewBufferString("AK ADAK NAS               ADK   70454  51 53N  176 39W    3   X     T          7 US"),
			out:  map[string]Station{},
		},
		{
			name: "invalid line",
			in:   bytes.NewBufferString(" "),
			out:  map[string]Station{},
		},
		{
			name: "commented",
			in:   bytes.NewBufferString("!  ADAK NAS         PADK  ADK   70454  51 53N  176 39W    3   X     T          7 US"),
			out:  map[string]Station{},
		},
		{
			name: "bogus coordinates",
			in:   bytes.NewBufferString("AK ADAK NAS         PADK  ADK   70454  51053N  176039W    3   X     T          7 US"),
			out: map[string]Station{
				"PADK": {State: "AK", Name: "ADAK NAS", ICAO: "PADK", Country: "US", Coordinates: nil},
			},
			errors: 1,
		},
		{
			name: "duplicate",
			in:   bytes.NewBufferString("AK ADAK NAS         PADK  ADK   70454  51 53N  176 39W    3   X     T          7 US\nAK ADAK NAS         PADK  ADK   70454  51 53N  176 39W    3   X     T          7 US"),
			out: map[string]Station{
				"PADK": {State: "AK", Name: "ADAK NAS", ICAO: "PADK", Country: "US", Coordinates: ptr(t, geo.NewCoordinates(51.883333, -176.650000))},
			},
			errors: 1,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			stations, errs := Parse(tt.in)
			if errl := len(errs); errl != tt.errors {
				t.Fatalf("Expected: %d errors, got: %d, %v", tt.errors, errl, errs)
			}
			if stationsl := len(stations); stationsl != len(tt.out) {
				t.Fatalf("Expected: %d stations, got %d, %v", len(tt.out), stationsl, stations)
			}
			for i, s := range stations {
				sres := s
				sexp := tt.out[i]
				if sres.Name != sexp.Name {
					t.Fatalf("Expected name: '%s', got: '%s'", sexp.Name, sres.Name)
				}
				if sres.ICAO != sexp.ICAO {
					t.Fatalf("Expected ICAO: '%s', got: '%s'", sexp.ICAO, sres.ICAO)
				}
				if sres.Country != sexp.Country {
					t.Fatalf("Expected country: '%s', got: '%s'", sexp.Country, sres.Country)
				}
				if sres.State != sexp.State {
					t.Fatalf("Expected state: '%s', got: '%s'", sexp.State, sres.State)
				}
				if sexp.Coordinates != nil {
					if sres.Coordinates == nil {
						t.Fatalf("Excepted coordinates: '%f %f', got: nil", sexp.Coordinates.Lat, sexp.Coordinates.Lon)
					}
					if sexp.Coordinates.Lat != sres.Coordinates.Lat {
						t.Fatalf("Expected latitude of: '%f', got: '%f'", sexp.Coordinates.Lat, sres.Coordinates.Lat)
					}
					if sexp.Coordinates.Lon != sres.Coordinates.Lon {
						t.Fatalf("Expected longitude of: '%f', got: '%f'", sexp.Coordinates.Lon, sres.Coordinates.Lon)
					}
				}
				if sexp.Coordinates == nil && sres.Coordinates != nil {
					t.Fatalf("Did not expect coordinates, got: '%f %f'", sres.Coordinates.Lat, sres.Coordinates.Lon)
				}
			}
		})
	}
}

func shouldPanic(t *testing.T, f func(data io.Reader) Stations, data io.Reader) {
	t.Helper()
	defer func() { _ = recover() }()
	f(data)
	t.Errorf("should have panicked")
}

func TestMustParse(t *testing.T) {
	tests := []struct {
		name  string
		in    io.Reader
		panic bool
	}{
		{
			name:  "empty",
			in:    bytes.NewBufferString(""),
			panic: false,
		},
		{
			name:  "valid",
			in:    bytes.NewBufferString("AK ADAK NAS         PADK  ADK   70454  51 53N  176 39W    3   X     T          7 US"),
			panic: false,
		},
		{
			name:  "bogus",
			in:    bytes.NewBufferString("AK ADAK NAS         PADK  ADK   70454  51053N  176 39W    3   X     T          7 US"),
			panic: true,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			if tt.panic {
				shouldPanic(t, MustParse, tt.in)
			} else {
				_ = MustParse(tt.in)
			}
		})
	}
}
