package validation

import (
	"bytes"
	"strings"
	"testing"

	"gitlab.gnome.org/daenney/castellanus/internal/geo"
	"gitlab.gnome.org/daenney/castellanus/internal/libgweather"
	"gitlab.gnome.org/daenney/castellanus/internal/metar"
	"gitlab.gnome.org/daenney/castellanus/internal/osm"
)

func ptr[T any](t *testing.T, x T) *T {
	t.Helper()
	return &x
}

func TestGroupCheck(t *testing.T) {
	ruleNoopSuccess := func(libgweather.Entry) Result {
		return Result{Success: true, Msg: "noop"}
	}

	ruleNoopTerminate := func(libgweather.Entry) Result {
		return Result{Terminate: true, Msg: "terminate"}
	}

	tests := []struct {
		name     string
		group    Group
		ok       bool
		expected int
	}{
		{
			name:     "some rules",
			group:    Group{Rules: []Func{ruleNoopSuccess, ruleNoopSuccess}},
			expected: 2,
			ok:       true,
		},
		{
			name:     "with termination",
			group:    Group{Rules: []Func{ruleNoopSuccess, ruleNoopTerminate, ruleNoopSuccess}},
			expected: 2,
			ok:       false,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res, ok := tt.group.Check(libgweather.Entry{})
			if ok != tt.ok {
				t.Fatalf("Expected ok: %t, got: %t", tt.ok, ok)
			}
			if lres := len(res); lres != tt.expected {
				t.Fatalf("Expected: %d results, got: %d", tt.expected, lres)
			}
		})
	}
}

func TestLocationMETAR(t *testing.T) {
	tests := []struct {
		name     string
		stations metar.Stations
		entry    libgweather.Entry
		res      Result
	}{
		{
			name:     "no matching station",
			stations: make(map[string]metar.Station, 0),
			entry:    libgweather.Entry{METAR: ptr(t, "AAAA")},
			res:      ErrorMETAR,
		},
		{
			name:     "matching station",
			stations: map[string]metar.Station{"AAAA": {ICAO: "AAAA"}},
			entry:    libgweather.Entry{METAR: ptr(t, "AAAA")},
			res:      SuccessMETAR,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res := LocationMETAR(tt.stations)(tt.entry)
			if !res.Is(tt.res) {
				t.Fatalf("Expected result: %#v, got: %#v", tt.res, res)
			}
		})
	}
}

func TestLocationRadius(t *testing.T) {
	tests := []struct {
		name     string
		stations metar.Stations
		entry    libgweather.Entry
		res      Result
	}{
		{
			name:     "no matching station",
			stations: make(map[string]metar.Station, 0),
			entry:    libgweather.Entry{METAR: ptr(t, "AAAA")},
			res:      ErrorRadius,
		},
		{
			name:     "within radius",
			stations: map[string]metar.Station{"AAAA": {Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))}},
			entry:    libgweather.Entry{METAR: ptr(t, "AAAA"), Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
			res:      SuccessRadius,
		},
		{
			name:     "beyond radius",
			stations: map[string]metar.Station{"AAAA": {Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))}},
			entry:    libgweather.Entry{METAR: ptr(t, "AAAA"), Coordinates: ptr(t, geo.NewCoordinates(90.0, 1.0))},
			res:      ErrorRadius,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res := LocationRadius(2.0, tt.stations)(tt.entry)
			if !res.Is(tt.res) {
				t.Fatalf("Expected result: %#v, got: %#v", tt.res, res)
			}
		})
	}
}

func TestCityOSMAvailable(t *testing.T) {
	tests := []struct {
		name  string
		entry libgweather.Entry
		data  osm.Info
		res   Result
	}{
		{
			name:  "no data",
			data:  osm.Info{},
			entry: libgweather.Entry{},
			res:   ErrorOSM,
		},
		{
			name:  "with data",
			data:  osm.Info{Name: "Athens"},
			entry: libgweather.Entry{},
			res:   SuccessOSM,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res := CityOSMAvailable(tt.data)(tt.entry)
			if !res.Is(tt.res) {
				t.Fatalf("Expected result: %#v, got: %#v", tt.res, res)
			}
		})
	}
}

func TestCityName(t *testing.T) {
	tests := []struct {
		name  string
		entry libgweather.Entry
		data  osm.Info
		res   Result
	}{
		{
			name:  "with matching city",
			data:  osm.Info{Name: "Athens"},
			entry: libgweather.Entry{Name: "Athens"},
			res:   SuccessCityName,
		},
		{
			name:  "without matching city",
			data:  osm.Info{Name: "Thessaloniki"},
			entry: libgweather.Entry{Name: "Athens"},
			res:   Result{Warn: true, Msg: "name does not match OSM data", Source: ptr(t, "Athens"), Retrieved: ptr(t, "Thessaloniki")},
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res := CityName(tt.data)(tt.entry)
			if !res.Is(tt.res) {
				t.Fatalf("Expected result: %#v, got: %#v", tt.res, res)
			}
		})
	}
}

func TestCityRadius(t *testing.T) {
	tests := []struct {
		name  string
		data  osm.Info
		entry libgweather.Entry
		res   Result
	}{
		{
			name:  "no data",
			data:  osm.Info{},
			entry: libgweather.Entry{},
			res:   ErrorRadius,
		},
		{
			name:  "within radius",
			data:  osm.Info{Coordinates: geo.NewCoordinates(1.0, 1.0)},
			entry: libgweather.Entry{Coordinates: ptr(t, geo.NewCoordinates(1.0, 1.0))},
			res:   SuccessRadius,
		},
		{
			name:  "beyond radius",
			data:  osm.Info{Coordinates: geo.NewCoordinates(1.0, 1.0)},
			entry: libgweather.Entry{Coordinates: ptr(t, geo.NewCoordinates(90.0, 1.0))},
			res:   ErrorRadius,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res := CityRadius(2.0, tt.data)(tt.entry)
			if !res.Is(tt.res) {
				t.Fatalf("Expected result: %#v, got: %#v", tt.res, res)
			}
		})
	}
}

func TestCityPopulationThreshold(t *testing.T) {
	tests := []struct {
		name  string
		entry libgweather.Entry
		data  osm.Info
		res   Result
	}{
		{
			name:  "no population data",
			data:  osm.Info{},
			entry: libgweather.Entry{Name: "Athens"},
			res:   UnavailablePopulation,
		},
		{
			name:  "below threshold",
			data:  osm.Info{Population: ptr[uint64](t, 100)},
			entry: libgweather.Entry{},
			res:   ErrorPopulationThreshold,
		},
		{
			name:  "above threshold",
			data:  osm.Info{Population: ptr[uint64](t, 5000)},
			entry: libgweather.Entry{},
			res:   SuccessPopulationThreshold,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res := CityPopulationThreshold(1000, tt.data)(tt.entry)
			if !res.Is(tt.res) {
				t.Fatalf("Expected result: %#v, got: %#v", tt.res, res)
			}
		})
	}
}

func TestCityPopulationRange(t *testing.T) {
	tests := []struct {
		name  string
		entry libgweather.Entry
		data  osm.Info
		res   Result
	}{
		{
			name:  "no OSM population data",
			data:  osm.Info{},
			entry: libgweather.Entry{Population: ptr[uint64](t, 100_000)},
			res:   ErrorPopulationRange,
		},
		{
			name:  "with population data in range",
			data:  osm.Info{Population: ptr[uint64](t, 100_000)},
			entry: libgweather.Entry{Population: ptr[uint64](t, 90_000)},
			res:   SuccessPopulationRange,
		},
		{
			name:  "with population data outside of range",
			data:  osm.Info{Population: ptr[uint64](t, 10_000)},
			entry: libgweather.Entry{Population: ptr[uint64](t, 90_000)},
			res:   ErrorPopulationRange,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res := CityPopulationRange(tt.data)(tt.entry)
			if !res.Is(tt.res) {
				t.Fatalf("Expected result: %#v, got: %#v", tt.res, res)
			}
		})
	}
}

func TestIsState(t *testing.T) {
	tests := []struct {
		name       string
		in         Result
		successful bool
		warning    bool
		failure    bool
	}{
		{
			name:       "success and not warning",
			in:         Result{Success: true},
			successful: true,
		},
		{
			name:    "succes and warn",
			in:      Result{Success: true, Warn: true},
			warning: true,
		},
		{
			name:    "failure and not warning",
			in:      Result{},
			failure: true,
		},
		{
			name:    "failure and warning",
			in:      Result{Warn: true},
			warning: true,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			s := tt.in.IsSuccessful()
			w := tt.in.IsWarning()
			f := tt.in.IsFailure()
			if s != tt.successful {
				t.Fatalf("Expected success: %t, got: %t", tt.successful, s)
			}
			if w != tt.warning {
				t.Fatalf("Expected warning: %t, got: %t", tt.warning, w)
			}
			if f != tt.failure {
				t.Fatalf("Expected failure: %t, got: %t", tt.failure, f)
			}
		})
	}
}

func TestOutputWriters(t *testing.T) {
	tests := []struct {
		name   string
		result Result
		plain  string
		fancy  string
	}{
		{
			name:   "success",
			result: Result{Success: true, Msg: "test"},
			plain: `success:
- [ ok ] test` + "\n",
			fancy: `success:
	✅ test` + "\n",
		},
		{
			name:   "warning",
			result: Result{Warn: true, Msg: "test"},
			plain: `warning:
- [warn] test` + "\n",
			fancy: `warning:
	🚩 test` + "\n",
		},
		{
			name:   "warning with data",
			result: Result{Warn: true, Msg: "test", Source: ptr(t, "a"), Retrieved: ptr(t, "b")},
			plain: `warning with data:
- [warn] test (source: a, retrieved: b)` + "\n",
			fancy: `warning with data:
	🚩 test (source: a, retrieved: b)` + "\n",
		},
		{
			name:   "failure",
			result: Result{Msg: "test"},
			plain: `failure:
- [fail] test` + "\n",
			fancy: `failure:
	❌ test` + "\n",
		},
		{
			name:   "failure with data",
			result: Result{Msg: "test", Source: ptr(t, "a"), Retrieved: ptr(t, "b")},
			plain: `failure with data:
- [fail] test (source: a, retrieved: b)` + "\n",
			fancy: `failure with data:
	❌ test (source: a, retrieved: b)` + "\n",
		},
		{
			name:   "failure with terminate",
			result: Result{Msg: "test", Terminate: true},
			plain: `failure with terminate:
- [fail] test
- [warn] Other checks have not been run` + "\n",
			fancy: `failure with terminate:
	❌ test
	📢 Other checks have not been run` + "\n",
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			wp := &bytes.Buffer{}
			ConsoleWriter(wp, tt.name, []Result{tt.result})
			resp := wp.String()
			if !strings.EqualFold(resp, tt.plain) {
				t.Fatalf("Expected: %s, got: %s", tt.plain, resp)
			}

			wf := &bytes.Buffer{}
			FancyConsoleWriter(wf, tt.name, []Result{tt.result})
			resf := wf.String()
			if !strings.EqualFold(resf, tt.fancy) {
				t.Fatalf("Expected: %s, got: %s", tt.fancy, resf)
			}
		})
	}
}

func TestResultWith(t *testing.T) {
	res := ErrorMETAR
	r := res.With("a", "b")
	if &res == &r {
		t.Fatalf("Did not expect equality, but got: %p == %p", &res, &r)
	}
	if res.Source != nil || res.Retrieved != nil {
		t.Fatalf("The Source and Retrieved fields should be nil on the original")
	}
	if r.Source == nil || r.Retrieved == nil {
		t.Fatalf("The Source and Retrieved fields should not be nil on the specialised Result")
	}
}

func TestResultIs(t *testing.T) {
	tests := []struct {
		name        string
		left, right Result
		matches     bool
	}{
		{
			name:    "matches with single specialised Result",
			left:    ErrorMETAR,
			right:   ErrorMETAR.With("a", "b"),
			matches: true,
		},
		{
			name:    "matches with both specialised Results",
			left:    ErrorMETAR.With("a", "b"),
			right:   ErrorMETAR.With("c", "d"),
			matches: true,
		},
		{
			name:    "does not match without specialised Results",
			left:    ErrorMETAR,
			right:   ErrorOSM,
			matches: false,
		},
		{
			name:    "does not match with single specialised Result",
			left:    ErrorMETAR,
			right:   ErrorOSM.With("a", "b"),
			matches: false,
		},
		{
			name:    "does not match with both specialised Results",
			left:    ErrorMETAR.With("a", "b"),
			right:   ErrorOSM.With("a", "b"),
			matches: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res := tt.left.Is(tt.right)
			if res != tt.matches {
				t.Fatalf("Expected: %t, got: %t", tt.matches, res)
			}
		})
	}
}
