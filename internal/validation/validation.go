package validation

import (
	"fmt"
	"io"
	"strconv"
	"strings"

	"gitlab.gnome.org/daenney/castellanus/internal/libgweather"
	"gitlab.gnome.org/daenney/castellanus/internal/metar"
	"gitlab.gnome.org/daenney/castellanus/internal/osm"
)

// Result is the result of the Funcs applied to a libgweather.Entry
//
// Validations exist in 3 forms, successful, warning or failure. Success
// and failure is just the Success state, Warn has its own key. If a Result
// has Success set to false but Warn set to true, it should be treated as a
// warning, not a failure. Checking for failure comes last. Use the IsXXX
// methods to be sure.
//
// When the Terminate flag is set to true, further Results in the Group
// should be ignored. Or as an optimisation, not run any of the Rules after
// the one that emitted a Result with Terminate set. Only Result's that
// evaluate to having failed will honour the termination flag.
//
// The Source and Retrieved attributes can be used to specialise the output
// in case of a warning or failure. They can be helpful to give an indication
// of what was wrong, or when the input data doesn't end up matching with
// the data retrieved from a data source.
//
// As a rule of thumb, Validations that are heavily dependent on some human
// context that's difficult to universally correctly match, like a name, should
// prefer to return a Result with Warn set.
type Result struct {
	Success   bool
	Warn      bool
	Terminate bool
	Msg       string
	Retrieved *string
	Source    *string
}

func (r Result) IsSuccessful() bool {
	return r.Success && !r.Warn
}

func (r Result) IsWarning() bool {
	return r.Warn
}

func (r Result) IsFailure() bool {
	return !r.Success && !r.Warn
}

// With lets us specialise a Result by passing in additional data
func (r Result) With(src, ret string) Result {
	r.Source = &src
	r.Retrieved = &ret
	return r
}

// Is checks if one Result is the same as another. It does this by comparing
// all non-optional fields for equality
func (r Result) Is(o Result) bool {
	if r.Success == o.Success && r.Warn == o.Warn &&
		r.Terminate == o.Terminate && r.Msg == o.Msg {
		return true
	}
	return false
}

// Func is used to validate a libgweather.Entry
type Func func(e libgweather.Entry) Result

// Group holds a number of Funcs that will be executed in order
type Group struct {
	Rules []Func
}

// Check runs the group's ValidationFuncs for the provided entry
//
// If a rule returns with a non-successful rule, the returned boolean will be
// false to indicate at least one rule in the group failed. If a rule returns
// with terminate set to true, any rules following it will not be executed.
func (v Group) Check(e libgweather.Entry) ([]Result, bool) {
	res := make([]Result, 0, len(v.Rules))
	ok := true
	for _, r := range v.Rules {
		val := r(e)
		if !val.Success {
			ok = false
		}
		res = append(res, val)
		if val.Terminate {
			break
		}
	}
	return res, ok
}

func formatNum(number uint64) string {
	output := strconv.FormatUint(number, 10)
	startOffset := 3
	for outputIndex := len(output); outputIndex > startOffset; {
		outputIndex -= 3
		output = output[:outputIndex] + " " + output[outputIndex:]
	}
	return output
}

var (
	SuccessMETAR               = Result{Success: true, Msg: "METAR data is available"}
	SuccessOSM                 = Result{Success: true, Msg: "OSM data is available"}
	SuccessCityName            = Result{Success: true, Msg: "name matches OSM data"}
	SuccessRadius              = Result{Success: true, Msg: "coordinates are within the required radius"}
	SuccessPopulationThreshold = Result{Success: true, Msg: "meets or exceeds population threshold"}
	SuccessPopulationRange     = Result{Success: true, Msg: "is within range of OSM population data"}

	UnavailablePopulation       = Result{Warn: true, Msg: "population data from OSM is not available"}
	UnavailablePopulationSource = Result{Warn: true, Msg: "no population data declared on entry"}
	UnavailableNameMatch        = Result{Warn: true, Msg: "name does not match OSM data"}

	ErrorMETAR               = Result{Terminate: true, Msg: "METAR data is unavailable"}
	ErrorOSM                 = Result{Terminate: true, Msg: "OSM data is unavailable"}
	ErrorRadius              = Result{Msg: "coordinates are outside of required radius"}
	ErrorPopulationThreshold = Result{Msg: "does not meet population threshold"}
	ErrorPopulationRange     = Result{Msg: "is outside the range of OSM population data"}
)

// LocationMETAR checks if there's a known METAR station matching
// the ICAO code of the entry
func LocationMETAR(stations metar.Stations) Func {
	return func(e libgweather.Entry) Result {
		_, ok := stations[*e.METAR]
		if ok {
			return SuccessMETAR
		}
		return ErrorMETAR
	}
}

// LocationRadius checks if the entry coordinates match the METAR
// station coordinates within a certain radius
func LocationRadius(radius float64, stations metar.Stations) Func {
	return func(e libgweather.Entry) Result {
		v, ok := stations[*e.METAR]
		if ok {
			if e.Coordinates != nil {
				if e.Coordinates.Distance(*v.Coordinates) <= radius {
					return SuccessRadius
				}
				return ErrorRadius.With(e.Coordinates.String(), v.Coordinates.String())
			}
		}
		return ErrorRadius
	}
}

// RulesForLocation returns a set of rules that can be added to a Group
// for validating location entries
func RulesForLocation(radius float64, stations metar.Stations) []Func {
	return []Func{
		LocationMETAR(stations),
		LocationRadius(radius, stations),
	}
}

// CityOSMAvailable checks that we've managed to retrieve data from OSM
// for a particular entry
func CityOSMAvailable(data osm.Info) Func {
	return func(e libgweather.Entry) Result {
		if data.Name != "" {
			return SuccessOSM
		}
		return ErrorOSM
	}
}

// CityName checks that the name on the entry matches the name from
// the OSM data
func CityName(data osm.Info) Func {
	return func(e libgweather.Entry) Result {
		if strings.EqualFold(e.Name, data.Name) {
			return SuccessCityName
		}
		return UnavailableNameMatch.With(e.Name, data.Name)
	}
}

// CityRadius checks if the entry coordinates match the coordinates
// from OSM within a certain radius
func CityRadius(radius float64, data osm.Info) Func {
	return func(e libgweather.Entry) Result {
		if e.Coordinates != nil {
			if e.Coordinates.Distance(data.Coordinates) <= radius {
				return SuccessRadius
			}
			return ErrorRadius.With(e.Coordinates.String(), data.Coordinates.String())
		}
		return ErrorRadius
	}
}

// CityPopulationThreshold checks if the entry meets the minimum
// inclusion threshold if OSM population data is available
func CityPopulationThreshold(population uint64, data osm.Info) Func {
	return func(e libgweather.Entry) Result {
		if data.Population != nil {
			if *data.Population >= population {
				return SuccessPopulationThreshold
			}
			return ErrorPopulationThreshold.With(fmt.Sprint(formatNum(population)), fmt.Sprint(formatNum(*data.Population)))
		}
		return UnavailablePopulation
	}
}

// CityPopulationRange checks that the stated population on the
// entry is within range of the available OSM population data
func CityPopulationRange(data osm.Info) Func {
	return func(e libgweather.Entry) Result {
		if e.Population == nil {
			return UnavailablePopulationSource
		}
		if data.Population != nil {
			if *data.Population <= *e.Population+50_000 && *e.Population >= *data.Population-50_000 {
				return SuccessPopulationRange
			}
		}
		return ErrorPopulationRange
	}
}

// RulesForCity returns a set of rules that can be added to a Group for
// validating city entries
func RulesForCity(radius float64, population uint64, data osm.Info) []Func {
	return []Func{
		CityOSMAvailable(data),
		CityName(data),
		CityRadius(radius, data),
		CityPopulationThreshold(population, data),
		// CityPopulationRange(data), // Add this in once we start tracking population on <city> entries
	}
}

type Writer func(w io.Writer, name string, results []Result)

func ConsoleWriter(w io.Writer, name string, results []Result) {
	s := strings.Builder{}
	s.WriteString(name + ":\n")
	for _, val := range results {
		switch {
		case val.IsSuccessful():
			s.WriteString("- [ ok ] " + val.Msg + "\n")
		case val.IsWarning():
			s.WriteString("- [warn] " + val.Msg)
			if val.Source != nil && val.Retrieved != nil {
				s.WriteString(fmt.Sprintf(" (source: %s, retrieved: %s)", *val.Source, *val.Retrieved))
			}
			s.WriteString("\n")
		case val.IsFailure():
			s.WriteString("- [fail] " + val.Msg)
			if val.Source != nil && val.Retrieved != nil {
				s.WriteString(fmt.Sprintf(" (source: %s, retrieved: %s)", *val.Source, *val.Retrieved))
			}
			s.WriteString("\n")
			if val.Terminate {
				s.WriteString("- [warn] Other checks have not been run\n")
			}
		}
	}
	w.Write([]byte(s.String()))
}

func FancyConsoleWriter(w io.Writer, name string, results []Result) {
	s := strings.Builder{}
	s.WriteString(name + ":\n")
	for _, val := range results {
		switch {
		case val.IsSuccessful():
			s.WriteString("\t✅ " + val.Msg + "\n")
		case val.IsWarning():
			s.WriteString("\t🚩 " + val.Msg)
			if val.Source != nil && val.Retrieved != nil {
				s.WriteString(fmt.Sprintf(" (source: %s, retrieved: %s)", *val.Source, *val.Retrieved))
			}
			s.WriteString("\n")
		case val.IsFailure():
			s.WriteString("\t❌ " + val.Msg)
			if val.Source != nil && val.Retrieved != nil {
				s.WriteString(fmt.Sprintf(" (source: %s, retrieved: %s)", *val.Source, *val.Retrieved))
			}
			s.WriteString("\n")
			if val.Terminate {
				s.WriteString("\t📢 Other checks have not been run\n")
			}
		}
	}
	w.Write([]byte(s.String()))
}
