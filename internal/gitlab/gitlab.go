package gitlab

import (
	"errors"
	"fmt"
	"net/url"

	ihttp "gitlab.gnome.org/daenney/castellanus/internal/http"
)

const (
	LibgweatherRepository = "https://gitlab.gnome.org/api/v4/projects/294"
)

var (
	ErrorUnexpectedStatus = errors.New("unexpected status code")
	ErrorInvalidJSON      = errors.New("invalid or incomplete JSON")
)

func GetFile(baseURL string, cl ihttp.Client, ref string) ([]byte, error) {
	base, err := url.Parse(baseURL + "/repository/files/data%2FLocations.xml/raw")
	if err != nil {
		return nil, err
	}

	if ref == "" {
		ref = "main"
	}
	params := url.Values{}
	params.Add("ref", ref)
	base.RawQuery = params.Encode()

	return ihttp.GetRaw(base.String(), cl)
}

// GetMRData returns the base commit hash as well as the head commit hash of a
// merge request.
func GetMRData(baseURL string, cl ihttp.Client, mr int) (string, string, error) {
	base, err := url.Parse(baseURL + "/merge_requests/" + fmt.Sprint(mr))
	if err != nil {
		return "", "", err
	}

	data := mrDetails{}
	err = ihttp.GetJSON(base.String(), cl, &data)
	if err != nil {
		return "", "", err
	}

	return data.Diffs.BaseSHA, data.Diffs.HeadSHA, nil
}

type mrDetails struct {
	Diffs struct {
		BaseSHA string `json:"base_sha"`
		HeadSHA string `json:"head_sha"`
	} `json:"diff_refs"`
}
