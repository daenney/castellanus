package generate

import (
	"errors"
	"fmt"
	"log"
	"net/url"
	"os"
	"strings"
	"text/template"
	"time"

	ihttp "gitlab.gnome.org/daenney/castellanus/internal/http"
	"gitlab.gnome.org/daenney/castellanus/internal/osm"
	"golang.org/x/time/rate"
)

type Cmd struct {
	OSMURL    []string `arg:"positional,required" placeholder:"URL" help:"OSM URL to generate location or city from"`
	UAVersion string   `arg:"-"`
}

const Description = "The generate subcommand will generate a location or city XML " +
	"stanza based on a provided OpenStreetMap URL. " +
	"This command expects to be given an OSM URL of the form: " +
	"https://www.openstreetmap.org/<type>/<ID>. The type will be one of " +
	"N(ode), W(ay) or R(elation). The ID is a sequence of numbers. " +
	"The protocol and hostname may be omitted, as well as the first '/', " +
	"so values like /<type>/<ID> or <type>/<ID> are also valid. The URL " +
	"can also include query parameters or URL fragments.\n\n" +

	"To find the right URL, head to https://www.openstreetmap.org and search for something " +
	"in the search box. Pick the best matching result and use that URL as an argument to this command. " +
	"If a provided URL cannot be determined to be an airport, we'll assume this is a city.\n\n" +

	"For cities, ensure you supply a URL that points to the city, and not a similarly named " +
	"municipality. In some cases, you'll want to pick the Village or Village Boundary instead of the City " +
	"from the provided search results.\n\n" +

	"For airports, ensure you supply a URL that points to the airport, and not an airport " +
	"terminal or adjacent transit location (like a bus stop or train station).\n\n" +

	"For each provided URL, it will try to fetch details about the object and generate " +
	"a <location> or <city> XML stanza suitable for inclusion in libgweather.\n"

const (
	tplLocation = `<location>
	<name>{{ .Name }}</name>
	<code>{{ .Code }}</code>
	<coordinates>{{ .Coordinates }}</coordinates>
</location>
`
	tplCity = `<city>
	<_name>{{ .Name }}</_name>
	<coordinates>{{ .Coordinates }}</coordinates>
</city>
`
)

var (
	loc  = template.Must(template.New("location").Parse(tplLocation))
	city = template.Must(template.New("city").Parse(tplCity))
)

func (cmd *Cmd) Run() {
	if len(cmd.OSMURL) == 0 {
		os.Exit(0)
	}

	ids := make([]string, 0, len(cmd.OSMURL))
	for _, url := range cmd.OSMURL {
		id, err := parseURL(url)
		if err != nil {
			log.Println(err)
			continue
		}
		ids = append(ids, id)
	}

	osmc := ihttp.NewClient(rate.NewLimiter(rate.Every(time.Second), 1), cmd.UAVersion)
	data, err := osm.LookupObject(osm.BaseURL, osmc, ids...)
	if err != nil {
		if errors.Is(err, osm.ErrorNoMatch) {
			log.Printf("no data could be retrieved for URL(s): %s\n", strings.Join(cmd.OSMURL, " "))
			os.Exit(0)
		}
		log.Fatalln(err)
	}

	if len(data) != len(cmd.OSMURL) {
		log.Println("failed to retrieve data for some URLs")
	}

	type tplData struct {
		Name        string
		Code        string
		Coordinates string
	}

	for i, d := range data {
		if d.ICAO != nil {
			loc.Execute(os.Stdout, tplData{Name: d.Name, Code: *d.ICAO, Coordinates: d.Coordinates.String()})
		} else {
			city.Execute(os.Stdout, tplData{Name: d.Name, Coordinates: d.Coordinates.String()})
		}
		if i != len(data)-1 {
			os.Stdout.WriteString("\n")
		}
	}
}

func parseURL(in string) (string, error) {
	res, err := url.Parse(in)
	if err != nil {
		return "", err
	}

	kind, id, found := strings.Cut(strings.TrimPrefix(res.Path, "/"), "/")
	if !found {
		return "", fmt.Errorf("provided string doesn't look like an OSM URL")
	}

	return strings.ToUpper(string(kind[0])) + id, nil
}
