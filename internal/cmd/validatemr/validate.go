package validatemr

import (
	"errors"
	"fmt"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.gnome.org/daenney/castellanus/internal/cmd/validate"
	"gitlab.gnome.org/daenney/castellanus/internal/gitlab"
	ihttp "gitlab.gnome.org/daenney/castellanus/internal/http"
	"gitlab.gnome.org/daenney/castellanus/internal/osm"
	"gitlab.gnome.org/daenney/castellanus/internal/validation"
	"golang.org/x/time/rate"
)

type Cmd struct {
	MR []string `arg:"positional,required" help:"one or multiple Gitlab MRs to validate as !<number> or URL"`
	validate.SharedOpts
}

const Description = "The validate-mr subcommand does the exact same thing as the " +
	"validate subcommand but takes a Gitlab MR instead.\n\n" +
	"You can provide one or multiple MR <number>, !<number> or Gitlab URLs " +
	"to the MR. The proposed changes in the MR will be checked against the " +
	"Locations.xml from main.\n"

func (cmd *Cmd) Run() {
	if len(cmd.MR) == 0 {
		os.Exit(0)
	}

	gl := ihttp.NewClient(rate.NewLimiter(rate.Every(10*time.Second), 50), cmd.UAVersion)

	dir, err := os.MkdirTemp("", "castellanus-*")
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		os.RemoveAll(dir)
	}()

	ids := []int{}
	for _, mr := range cmd.MR {
		id, err := idFromMR(mr)
		if err != nil {
			log.Printf("failed to parse MR from: %s (%v)\n", mr, err)
			continue
		}
		base, head, err := gitlab.GetMRData(gitlab.LibgweatherRepository, gl, id)
		if err != nil {
			log.Printf("failed to find MR with id: %d (%v)\n", id, err)
			continue
		}

		basexml, err := gitlab.GetFile(gitlab.LibgweatherRepository, gl, base)
		if err != nil {
			log.Printf("failed to retrieve base Locations.xml from MR with id: %d (%v)\n", id, err)
			continue
		}
		err = os.WriteFile(filepath.Join(dir, "base.xml"), basexml, 0o644)
		if err != nil {
			log.Printf("failed to save base Locations.xml from MR with id: %d to file (%v)\n", id, err)
			continue
		}

		headxml, err := gitlab.GetFile(gitlab.LibgweatherRepository, gl, head)
		if err != nil {
			log.Printf("failed to retrieve head Locations.xml from MR with id: %d (%v)\n", id, err)
			continue
		}
		err = os.WriteFile(filepath.Join(dir, fmt.Sprintf("%d.xml", id)), headxml, 0o644)
		if err != nil {
			log.Printf("failed to save head Locations.xml from MR with id: %d to file (%v)\n", id, err)
			continue
		}
		ids = append(ids, id)
	}

	writer := validation.FancyConsoleWriter
	if cmd.OutputFormat != "fancy" {
		writer = validation.ConsoleWriter
	}

	osmc := ihttp.NewClient(rate.NewLimiter(rate.Every(time.Second), 1), cmd.UAVersion)

	ok := true
	for i, id := range ids {
		old, err := os.Open(filepath.Join(dir, "base.xml"))
		if err != nil {
			log.Printf("failed to read file: %s (%v)\n", filepath.Join(dir, "base.xml"), err)
			continue
		}
		defer old.Close()
		new, err := os.Open(filepath.Join(dir, fmt.Sprintf("%d.xml", id)))
		if err != nil {
			log.Printf("failed to read file: %s (%v)\n", filepath.Join(dir, fmt.Sprintf("%d.xml", id)), err)
			continue
		}
		defer new.Close()

		entries, err := validate.Changed(old, new)
		if err != nil {
			log.Printf("failed to check for changes: %v\n", err)
			continue
		}

		os.Stdout.WriteString("[Validation for MR: !" + fmt.Sprint(id) + "]\n")
		last := entries.Back()
		for el := entries.Front(); el != nil; el = el.Next() {
			var g validation.Group
			if el.Value.METAR != nil && cmd.ValidateLocation {
				g = validation.Group{Rules: validation.RulesForLocation(cmd.RadiusLocation, validate.Stations)}
			} else if el.Value.METAR == nil && cmd.ValidateCity {
				data, err := osm.GetCity(osm.BaseURL, osmc, el.Value)
				if err != nil {
					if !errors.Is(err, osm.ErrorNoMatch) {
						log.Println(err)
					}
				}
				g = validation.Group{Rules: validation.RulesForCity(cmd.RadiusCity, cmd.PopulationThreshold, data)}
			}
			res, rok := g.Check(el.Value)
			if !rok {
				ok = rok
			}
			writer(os.Stdout, el.Value.String(), res)
			if el != last {
				os.Stdout.WriteString("\n")
			}
			if i != len(ids)-1 {
				os.Stdout.WriteString("\n")
			}
		}
	}
	if !ok {
		os.RemoveAll(dir)
		os.Exit(1)
	}
}

func idFromMR(mr string) (int, error) {
	if strings.HasPrefix(mr, "!") {
		mr = mr[1:]
	} else if strings.HasPrefix(mr, "http://") {
		res, err := url.Parse(mr)
		if err != nil {
			return 0, err
		}
		parts := strings.Split(res.Path, "/")
		if len(parts) < 6 {
			return 0, fmt.Errorf("URL path is not an MR")
		}
		if parts[4] != "merge_requests" {
			return 0, fmt.Errorf("URL path is not an MR")
		}
		mr = parts[5]
	}

	i, err := strconv.ParseInt(mr, 0, 64)
	return int(i), err
}
