package validate

import (
	"bytes"
	"encoding/xml"
	"errors"
	"io"
	"log"
	"os"
	"time"

	ihttp "gitlab.gnome.org/daenney/castellanus/internal/http"
	"gitlab.gnome.org/daenney/castellanus/internal/libgweather"
	"gitlab.gnome.org/daenney/castellanus/internal/metar"
	"gitlab.gnome.org/daenney/castellanus/internal/osm"
	"gitlab.gnome.org/daenney/castellanus/internal/validation"
	"golang.org/x/time/rate"
)

type SharedOpts struct {
	PopulationThreshold uint64  `arg:"--population" default:"100_000" help:"minimum population threshold for inclusion into libgweather"`
	RadiusCity          float64 `arg:"--radius-city" placeholder:"RADIUS" default:"5.0" help:"radius in km within which the supplied coordinates need to match the OSM coordinates"`
	RadiusLocation      float64 `arg:"--radius-location" placeholder:"RADIUS" default:"2.0" help:"radius in km within which the supplied coordinates need to match the METAR station coordinates"`
	ValidateLocation    bool    `arg:"--validate-location" default:"true" help:"validate changed location entries"`
	ValidateCity        bool    `arg:"--validate-city" default:"true" help:"validate changed city entries"`
	OutputFormat        string  `arg:"--output-format" default:"fancy" help:"how to format the result (plain|fancy)"`
	UAVersion           string  `arg:"-"`
}

type Cmd struct {
	Orig string `arg:"positional,required" placeholder:"ORIGINAL" help:"original Locations.xml"`
	New  string `arg:"positional,required" help:"updated Locations.xml"`
	SharedOpts
}

const Description = "The validate subcommand lets you compare two versions of Locations.xml " +
	"and analyse them for changes. Any added or updated entries will have the " +
	"validation routines run for them.\n\n" +
	"When validating a location entry the specified code will be used to check " +
	"whether METAR data is available. The METAR station coordinates will be " +
	"used to check the accuracy of the provided coordinates.\n\n" +
	"When validating a city, the name, optional state and country will be used " +
	"to retrieve the data from OpenStreetMap. The names will be compared and a " +
	"check on if the provided coordinates are within a certain distance of the " +
	"coordinates returned by OSM will be performed. If available, the population " +
	"data will be checked to ensure it meets the minimum requirements for " +
	"inclusion in libgweather.\n"

var Stations = metar.MustParse(bytes.NewBufferString(metar.StationsTXT))

func (cmd *Cmd) Run() {
	if !cmd.ValidateCity && !cmd.ValidateLocation {
		os.Exit(0)
	}

	old, err := os.Open(cmd.Orig)
	if err != nil {
		log.Fatalln(err)
	}
	defer old.Close()

	new, err := os.Open(cmd.New)
	if err != nil {
		log.Fatalln(err)
	}
	defer new.Close()

	entries, err := Changed(old, new)
	if err != nil {
		log.Fatalln(err)
	}

	writer := validation.FancyConsoleWriter
	if cmd.OutputFormat != "fancy" {
		writer = validation.ConsoleWriter
	}

	osmc := ihttp.NewClient(rate.NewLimiter(rate.Every(time.Second), 1), cmd.UAVersion)

	ok := true
	last := entries.Back()
	for el := entries.Front(); el != nil; el = el.Next() {
		var g validation.Group
		if el.Value.METAR != nil && cmd.ValidateLocation {
			g = validation.Group{Rules: validation.RulesForLocation(cmd.RadiusLocation, Stations)}
		} else if el.Value.METAR == nil && cmd.ValidateCity {
			data, err := osm.GetCity(osm.BaseURL, osmc, el.Value)
			if err != nil {
				if !errors.Is(err, osm.ErrorNoMatch) {
					log.Println(err)
				}
			}
			g = validation.Group{Rules: validation.RulesForCity(cmd.RadiusCity, cmd.PopulationThreshold, data)}
		}
		res, rok := g.Check(el.Value)
		if !rok {
			ok = rok
		}
		writer(os.Stdout, el.Value.String(), res)
		if el != last {
			os.Stdout.WriteString("\n")
		}
	}
	if !ok {
		os.Exit(1)
	}
}

func parse(data io.Reader) (libgweather.GWeather, error) {
	d := xml.NewDecoder(data)

	var gweather libgweather.GWeather
	err := d.Decode(&gweather)

	return gweather, err
}

func Changed(old, new io.Reader) (*libgweather.Entries, error) {
	olddata, err := parse(old)
	if err != nil {
		return nil, err
	}
	newdata, err := parse(new)
	if err != nil {
		return nil, err
	}

	oldentries, err := olddata.ToEntries()
	if err != nil {
		return nil, err
	}
	newentries, err := newdata.ToEntries()
	if err != nil {
		return nil, err
	}
	return libgweather.Changed(oldentries, newentries), nil
}
