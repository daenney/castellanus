package http

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestUserAgent(t *testing.T) {
	version := "1.0.0"
	c := &http.Client{Transport: &transport{version: version}}
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if ua := r.Header.Get("user-agent"); ua != fmt.Sprintf(userAgent, version) {
			t.Fatalf("Expected UA of: %s, got: %s", fmt.Sprintf(userAgent, version), ua)
		}
	}))
	defer ts.Close()
	c.Get(ts.URL)
}
