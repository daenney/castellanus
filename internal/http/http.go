package http

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"

	"golang.org/x/time/rate"
)

const userAgent = "castellanus/%s (+https://gitlab.gnome.org/daenney/castellanus)"

var (
	ErrorUnexpectedStatus = errors.New("unexpected status code")
	ErrorInvalidJSON      = errors.New("invalid or incomplete JSON")
)

type transport struct {
	version string
}

func (tr *transport) RoundTrip(req *http.Request) (*http.Response, error) {
	req.Header.Set("User-Agent", fmt.Sprintf(userAgent, tr.version))
	return http.DefaultTransport.RoundTrip(req)
}

type limitedClient struct {
	client *http.Client
	rl     *rate.Limiter
}

func (c *limitedClient) Do(req *http.Request) (*http.Response, error) {
	err := c.rl.Wait(req.Context())
	if err != nil {
		return nil, err
	}
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (c *limitedClient) Get(url string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	return c.Do(req)
}

func GetJSON[T any](url string, cl Client, resp *T) error {
	res, err := cl.Get(url)
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("got a non-200 status code for: %s %w", url, ErrorUnexpectedStatus)
	}

	defer func() {
		io.Copy(io.Discard, res.Body)
		res.Body.Close()
	}()

	j := json.NewDecoder(res.Body)
	err = j.Decode(resp)
	if err != nil {
		return fmt.Errorf("unexpected API response for: %s %v %w", url, err, ErrorInvalidJSON)
	}

	return nil
}

func GetRaw(url string, cl Client) ([]byte, error) {
	res, err := cl.Get(url)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("got a non-200 status code for: %s %w", url, ErrorUnexpectedStatus)
	}

	defer func() {
		io.Copy(io.Discard, res.Body)
		res.Body.Close()
	}()

	return io.ReadAll(res.Body)
}

type Client interface {
	Do(req *http.Request) (*http.Response, error)
	Get(url string) (*http.Response, error)
}

func NewClient(rate *rate.Limiter, version string) *limitedClient {
	return &limitedClient{
		client: &http.Client{Transport: &transport{version: version}},
		rl:     rate,
	}
}
