package osm

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"gitlab.gnome.org/daenney/castellanus/internal/geo"
	ihttp "gitlab.gnome.org/daenney/castellanus/internal/http"
	"gitlab.gnome.org/daenney/castellanus/internal/libgweather"
)

func ptr[T any](t *testing.T, x T) *T {
	t.Helper()
	return &x
}

func setupServer(t *testing.T, handler http.Handler) (url string, teardown func(t *testing.T)) {
	s := httptest.NewServer(handler)

	return s.URL, func(t *testing.T) {
		s.Close()
	}
}

func TestGetCity(t *testing.T) {
	tests := []struct {
		name    string
		handler http.HandlerFunc
		entry   libgweather.Entry
		err     error
		res     Info
	}{
		{
			name:    "OSM server error",
			handler: func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(http.StatusBadGateway) },
			entry:   libgweather.Entry{Country: "Greece", Name: "Athens"},
			err:     ihttp.ErrorUnexpectedStatus,
			res:     Info{},
		},
		{
			name: "empty body",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(""))
			},
			entry: libgweather.Entry{Country: "Greece", Name: "Athens"},
			err:   ihttp.ErrorInvalidJSON,
			res:   Info{},
		},
		{
			name: "invalid JSON",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte("aaaa"))
			},
			entry: libgweather.Entry{Country: "Greece", Name: "Athens"},
			err:   ihttp.ErrorInvalidJSON,
			res:   Info{},
		},
		{
			name: "no match",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte("{}}"))
			},
			entry: libgweather.Entry{Country: "Greece", Name: "Athens"},
			err:   ErrorNoMatch,
			res:   Info{},
		},
		{
			name: "single response",
			handler: func(w http.ResponseWriter, r *http.Request) {
				if r.URL.Path != "/search" {
					w.Write([]byte("{}"))
					return
				}
				w.Write([]byte(`{
					"type": "FeatureCollection",
					"geocoding": {
					  "attribution": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
					  "licence": "ODbL"
					},
					"features": [
					  {
						"type": "Feature",
						"properties": {
						  "geocoding": {
							"place_id": 12345,
							"osm_type": "node",
							"osm_id": 67890,
							"osm_key": "place",
							"osm_value": "city",
							"type": "city",
							"label": "Athens, Municipality of Athens, Regional Unit of Central Athens, Attica, 104 31, Greece",
							"name": "Athens",
							"country": "Greece",
							"postcode": "104 31",
							"state": "Attica",
							"county": "Regional Unit of Central Athens",
							"city": "Municipality of Athens",
							"admin": {
							  "level4": "Attica",
							  "level5": "Attica",
							  "level6": "Regional Unit of Central Athens",
							  "level7": "Municipality of Athens",
							  "level2": "Athens"
							}
						  }
						},
						"geometry": {
						  "type": "Point",
						  "coordinates": [
							1.0,
							1.0
						  ]
						}
					  }
					]
				  }
				  `))
			},
			err:   nil,
			entry: libgweather.Entry{Country: "Greece", Name: "Athens"},
			res: Info{
				Name:        "Athens",
				Coordinates: geo.NewCoordinates(1.0, 1.0),
			},
		},
		{
			name: "two items with first one being desired",
			handler: func(w http.ResponseWriter, r *http.Request) {
				if r.URL.Path != "/search" {
					w.Write([]byte("{}"))
					return
				}
				w.Write([]byte(`{
					"type": "FeatureCollection",
					"geocoding": {
					  "attribution": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
					  "licence": "ODbL"
					},
					"features": [
					  {
						"type": "Feature",
						"properties": {
						  "geocoding": {
							"place_id": 12345,
							"osm_type": "node",
							"osm_id": 67890,
							"osm_key": "place",
							"osm_value": "city",
							"type": "city",
							"label": "Athens, Municipality of Athens, Regional Unit of Central Athens, Attica, 104 31, Greece",
							"name": "Athens",
							"country": "Greece",
							"postcode": "104 31",
							"state": "Attica",
							"county": "Regional Unit of Central Athens",
							"city": "Municipality of Athens",
							"admin": {
							  "level4": "Attica",
							  "level5": "Attica",
							  "level6": "Regional Unit of Central Athens",
							  "level7": "Municipality of Athens",
							  "level2": "Athens"
							}
						  }
						},
						"geometry": {
						  "type": "Point",
						  "coordinates": [
							1.0,
							1.0
						  ]
						}
					  },
					  {
						"type": "Feature",
						"properties": {
						  "geocoding": {
							"place_id": 67890,
							"osm_type": "relation",
							"osm_id": 12345,
							"osm_key": "boundary",
							"osm_value": "administrative",
							"type": "state",
							"label": "Attica, Greece",
							"name": "Attica",
							"country": "Greece",
							"admin": {
							  "level4": "Attica"
							}
						  }
						},
						"geometry": {
						  "type": "Point",
						  "coordinates": [
							2.0,
							2.0
						  ]
						}
					  }				  
					]
				  }
				  `))
			},
			err:   nil,
			entry: libgweather.Entry{Country: "Greece", Name: "Athens"},
			res: Info{
				Name:        "Athens",
				Coordinates: geo.NewCoordinates(1.0, 1.0),
			},
		},
		{
			name: "two items with second one being desired",
			handler: func(w http.ResponseWriter, r *http.Request) {
				if r.URL.Path != "/search" {
					w.Write([]byte("{}"))
					return
				}
				w.Write([]byte(`{
					"type": "FeatureCollection",
					"geocoding": {
					  "attribution": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
					  "licence": "ODbL"
					},
					"features": [
					  {
						"type": "Feature",
						"properties": {
						  "geocoding": {
							"place_id": 67890,
							"osm_type": "relation",
							"osm_id": 12345,
							"osm_key": "boundary",
							"osm_value": "administrative",
							"type": "state",
							"label": "Attica, Greece",
							"name": "Attica",
							"country": "Greece",
							"admin": {
							  "level4": "Attica"
							}
						  }
						},
						"geometry": {
						  "type": "Point",
						  "coordinates": [
							2.0,
							2.0
						  ]
						}
					  },
					  {
						"type": "Feature",
						"properties": {
						  "geocoding": {
							"place_id": 12345,
							"osm_type": "node",
							"osm_id": 67890,
							"osm_key": "place",
							"osm_value": "city",
							"type": "city",
							"label": "Athens, Municipality of Athens, Regional Unit of Central Athens, Attica, 104 31, Greece",
							"name": "Athens",
							"country": "Greece",
							"postcode": "104 31",
							"state": "Attica",
							"county": "Regional Unit of Central Athens",
							"city": "Municipality of Athens",
							"admin": {
							  "level4": "Attica",
							  "level5": "Attica",
							  "level6": "Regional Unit of Central Athens",
							  "level7": "Municipality of Athens",
							  "level2": "Athens"
							}
						  }
						},
						"geometry": {
						  "type": "Point",
						  "coordinates": [
							1.0,
							1.0
						  ]
						}
					  }				  
					]
				  }
				  `))
			},
			err:   nil,
			entry: libgweather.Entry{Country: "Greece", Name: "Athens"},
			res: Info{
				Name:        "Athens",
				Coordinates: geo.NewCoordinates(1.0, 1.0),
			},
		},
		{
			name: "single request with state",
			handler: func(w http.ResponseWriter, r *http.Request) {
				if r.URL.Path != "/search" {
					w.Write([]byte("{}"))
					return
				}
				if !r.URL.Query().Has("state") {
					t.Fatalf("Expected state query argument")
				}
				w.Write([]byte(`{
					"type": "FeatureCollection",
					"geocoding": {
					  "attribution": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
					  "licence": "ODbL"
					},
					"features": [
					  {
						"type": "Feature",
						"properties": {
						  "geocoding": {
							"place_id": 12345,
							"osm_type": "node",
							"osm_id": 67890,
							"osm_key": "place",
							"osm_value": "city",
							"type": "city",
							"label": "Athens, Municipality of Athens, Regional Unit of Central Athens, Attica, 104 31, Greece",
							"name": "Athens",
							"country": "Greece",
							"postcode": "104 31",
							"state": "Attica",
							"county": "Regional Unit of Central Athens",
							"city": "Municipality of Athens",
							"admin": {
							  "level4": "Attica",
							  "level5": "Attica",
							  "level6": "Regional Unit of Central Athens",
							  "level7": "Municipality of Athens",
							  "level2": "Athens"
							}
						  }
						},
						"geometry": {
						  "type": "Point",
						  "coordinates": [
							1.0,
							1.0
						  ]
						}
					  }
					]
				  }
				  `))
			},
			err:   nil,
			entry: libgweather.Entry{Country: "Greece", Name: "Athens", State: ptr(t, "Attica")},
			res: Info{
				Name:        "Athens",
				Coordinates: geo.NewCoordinates(1.0, 1.0),
			},
		},
		{
			name: "single request with state fallback",
			handler: func(w http.ResponseWriter, r *http.Request) {
				if r.URL.Path != "/search" {
					w.Write([]byte("{}"))
					return
				}
				if r.URL.Query().Has("state") {
					w.Write([]byte(`{
						"type": "FeatureCollection",
						"geocoding": {
						  "attribution": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
						  "licence": "ODbL"
						},
						"features": []
					}`))
				}
				w.Write([]byte(`{
					"type": "FeatureCollection",
					"geocoding": {
					  "attribution": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
					  "licence": "ODbL"
					},
					"features": [
					  {
						"type": "Feature",
						"properties": {
						  "geocoding": {
							"place_id": 12345,
							"osm_type": "node",
							"osm_id": 67890,
							"osm_key": "place",
							"osm_value": "city",
							"type": "city",
							"label": "Athens, Municipality of Athens, Regional Unit of Central Athens, Attica, 104 31, Greece",
							"name": "Athens",
							"country": "Greece",
							"postcode": "104 31",
							"state": "Attica",
							"county": "Regional Unit of Central Athens",
							"city": "Municipality of Athens",
							"admin": {
							  "level4": "Attica",
							  "level5": "Attica",
							  "level6": "Regional Unit of Central Athens",
							  "level7": "Municipality of Athens",
							  "level2": "Athens"
							}
						  }
						},
						"geometry": {
						  "type": "Point",
						  "coordinates": [
							1.0,
							1.0
						  ]
						}
					  }
					]
				  }
				  `))
			},
			err:   nil,
			entry: libgweather.Entry{Country: "Greece", Name: "Athens", State: ptr(t, "Attica")},
			res: Info{
				Name:        "Athens",
				Coordinates: geo.NewCoordinates(1.0, 1.0),
			},
		},
		{
			name: "single response with population data",
			handler: func(w http.ResponseWriter, r *http.Request) {
				if r.URL.Path != "/search" {
					w.Write([]byte(`{
						"extratags": {
						  "population": "3090508"
						}
					  }
					  `))
					return
				}
				w.Write([]byte(`{
					"type": "FeatureCollection",
					"geocoding": {
					  "attribution": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
					  "licence": "ODbL"
					},
					"features": [
					  {
						"type": "Feature",
						"properties": {
						  "geocoding": {
							"place_id": 12345,
							"osm_type": "node",
							"osm_id": 67890,
							"osm_key": "place",
							"osm_value": "city",
							"type": "city",
							"label": "Athens, Municipality of Athens, Regional Unit of Central Athens, Attica, 104 31, Greece",
							"name": "Athens",
							"country": "Greece",
							"postcode": "104 31",
							"state": "Attica",
							"county": "Regional Unit of Central Athens",
							"city": "Municipality of Athens",
							"admin": {
							  "level4": "Attica",
							  "level5": "Attica",
							  "level6": "Regional Unit of Central Athens",
							  "level7": "Municipality of Athens",
							  "level2": "Athens"
							}
						  }
						},
						"geometry": {
						  "type": "Point",
						  "coordinates": [
							1.0,
							1.0
						  ]
						}
					  }
					]
				  }
				  `))
			},
			err: nil,
			res: Info{
				Name:        "Athens",
				Coordinates: geo.NewCoordinates(1.0, 1.0),
				Population:  ptr[uint64](t, 3090508),
			},
		},
	}

	c := &http.Client{}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			url, teardown := setupServer(t, http.Handler(tt.handler))
			defer teardown(t)

			res, err := GetCity(url, c, tt.entry)
			if tt.err != nil && err == nil {
				t.Fatalf("Expected error %v, but got nil", tt.err)
			}
			if tt.err == nil && err != nil {
				t.Fatalf("Expected no error, but got: %v", err)
			}
			if tt.err != nil && err != nil {
				if !errors.Is(err, tt.err) {
					t.Fatalf("Expected error: %v, but got: %v", tt.err, err)
				}
			}
			if err == nil {
				if !reflect.DeepEqual(res, tt.res) {
					t.Fatalf("Expected: %#v, got: %#v", tt.res, res)
				}
			}
		})
	}
}

func newFeature(t *testing.T, name, city string) feature {
	t.Helper()

	ft := feature{}
	if name != "" {
		ft.Properties.Geocoding.Name = name
	}
	if city != "" {
		ft.Properties.Geocoding.City = city
	}
	return ft
}

func TestFeatureName(t *testing.T) {
	tests := []struct {
		name   string
		in     feature
		search string
		out    string
	}{
		{
			name:   "matching name",
			search: "Athens",
			in:     newFeature(t, "Athens", ""),
			out:    "Athens",
		},
		{
			name:   "matching city",
			search: "Athens",
			in:     newFeature(t, "Brussels", "Athens"),
			out:    "Athens",
		},
		{
			name:   "no match",
			search: "Brussels",
			in:     newFeature(t, "Athens", "Athens"),
			out:    "Athens",
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res := tt.in.Name(tt.search)
			if tt.out != res {
				t.Fatalf("Expected name: %s, got: %s", tt.out, res)
			}
		})
	}
}

func TestGetPopulation(t *testing.T) {
	tests := []struct {
		name    string
		handler http.HandlerFunc
		err     error
		res     uint64
	}{
		{
			name:    "OSM server error",
			handler: func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(http.StatusBadGateway) },
			err:     ihttp.ErrorUnexpectedStatus,
			res:     0,
		},
		{
			name: "empty body",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(""))
			},
			err: ihttp.ErrorInvalidJSON,
			res: 0,
		},
		{
			name: "invalid JSON",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte("aaaa"))
			},
			err: ihttp.ErrorInvalidJSON,
			res: 0,
		},
		{
			name: "no match",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte("{}}"))
			},
			err: ErrorNoPopulation,
			res: 0,
		},
		{
			name: "response with population data not a number",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`{
						"extratags": {
						  "population": "aaaa"
						}
					  }
					  `))
			},
			err: ErrorNoPopulation,
			res: 0,
		},
		{
			name: "response with extratags being a lie",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`{
						"extratags": []
					  }
					  `))
			},
			err: ErrorNoPopulation,
			res: 0,
		},
		{
			name: "response with population data",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`{
						"extratags": {
						  "population": "1000"
						}
					  }
					  `))
			},
			err: nil,
			res: 1000,
		},
	}

	c := &http.Client{}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			url, teardown := setupServer(t, http.Handler(tt.handler))
			defer teardown(t)

			res, err := getPopulation(url, c, "rel", "id", "class")
			if tt.err != nil && err == nil {
				t.Fatalf("Expected error %v, but got nil", tt.err)
			}
			if tt.err == nil && err != nil {
				t.Fatalf("Expected no error, but got: %v", err)
			}
			if tt.err != nil && err != nil {
				if !errors.Is(err, tt.err) {
					t.Fatalf("Expected error: %v, but got: %v", tt.err, err)
				}
			}
			if err == nil {
				if tt.res != res {
					t.Fatalf("Expected population: %d, got: %d", tt.res, res)
				}
			}
		})
	}
}

func TestLookupObject(t *testing.T) {
	athens := lookupResp{}
	athens.Class = "place"
	athens.Type = "city"
	athens.Lat = 1.0
	athens.Lon = 1.0
	athens.Namedetails.Name = "Αθήνα"
	athens.Namedetails.NameEn = "Athens"

	venizelos := lookupResp{}
	venizelos.Class = "aeroway"
	venizelos.Type = "aerodrome"
	venizelos.Lat = 1.0
	venizelos.Lon = 1.0
	venizelos.Namedetails.ICAO = "LGAV"
	venizelos.Namedetails.Name = "Διεθνής Αερολιμένας Αθηνών «Ελευθέριος Βενιζέλος»"
	venizelos.Namedetails.NameEn = "Athens International Airport"

	tests := []struct {
		name    string
		handler http.HandlerFunc
		err     error
		res     []Info
	}{
		{
			name:    "OSM server error",
			handler: func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(http.StatusBadGateway) },
			err:     ihttp.ErrorUnexpectedStatus,
		},
		{
			name: "empty body",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(""))
			},
			err: ihttp.ErrorInvalidJSON,
		},
		{
			name: "invalid JSON",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte("aaaa"))
			},
			err: ihttp.ErrorInvalidJSON,
		},
		{
			name: "no match",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte("[]"))
			},
			err: ErrorNoMatch,
		},
		{
			name: "response with airport",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`[
					{
					  "licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
					  "lat": "1.0",
					  "lon": "1.0",
					  "class": "aeroway",
					  "type": "aerodrome",
					  "namedetails": {
						"icao": "LGAV",
						"name": "Διεθνής Αερολιμένας Αθηνών «Ελευθέριος Βενιζέλος»",
						"name:en": "Athens International Airport"
					  }
					}
				  ]`))
			},
			err: nil,
			res: []Info{{Name: "Athens International Airport", Coordinates: geo.NewCoordinates(1.0, 1.0), ICAO: ptr(t, "LGAV")}},
		},
		{
			name: "response with city",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`[
					{
					  "licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
					  "lat": "1.0",
					  "lon": "1.0",
					  "class": "place",
					  "type": "city",
					  "namedetails": {
						"name": "Αθήνα",
						"name:en": "Athens"
					  }
					}
				  ]`))
			},
			err: nil,
			res: []Info{{Name: "Athens", Coordinates: geo.NewCoordinates(1.0, 1.0)}},
		},
		{
			name: "response with 2 items",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.Write([]byte(`[
					{
					  "licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
					  "lat": "1.0",
					  "lon": "1.0",
					  "class": "aeroway",
					  "type": "aerodrome",
					  "namedetails": {
						"icao": "LGAV",
						"name": "Διεθνής Αερολιμένας Αθηνών «Ελευθέριος Βενιζέλος»",
						"name:en": "Athens International Airport"
					  }
					},
					{
						"licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
						"lat": "1.0",
						"lon": "1.0",
						"class": "place",
						"type": "city",
						"namedetails": {
						  "name": "Αθήνα",
						  "name:en": "Athens"
						}
					  }
				  ]`))
			},
			err: nil,
			res: []Info{
				{Name: "Athens International Airport", Coordinates: geo.NewCoordinates(1.0, 1.0), ICAO: ptr(t, "LGAV")},
				{Name: "Athens", Coordinates: geo.NewCoordinates(1.0, 1.0)},
			},
		},
	}

	c := &http.Client{}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			url, teardown := setupServer(t, http.Handler(tt.handler))
			defer teardown(t)

			res, err := LookupObject(url, c, "W1")
			if tt.err != nil && err == nil {
				t.Fatalf("Expected error %v, but got nil", tt.err)
			}
			if tt.err == nil && err != nil {
				t.Fatalf("Expected no error, but got: %v", err)
			}
			if tt.err != nil && err != nil {
				if !errors.Is(err, tt.err) {
					t.Fatalf("Expected error: %v, but got: %v", tt.err, err)
				}
			}
			if err == nil {
				if len(tt.res) != len(res) {
					t.Fatalf("Expected %d responses, got: %d", len(tt.res), len(res))
				}
				if !reflect.DeepEqual(tt.res, res) {
					t.Fatalf("Expected response: %#v, got: %#v", tt.res, res)
				}
			}
		})
	}
}

func TestLookupRespName(t *testing.T) {
	nameEn := lookupResp{}
	nameEn.Namedetails.Name = "Haha"
	nameEn.Namedetails.NameEn = "Hehe"

	name := lookupResp{}
	name.Namedetails.Name = "Haha"

	tests := []struct {
		name string
		res  string
		in   lookupResp
	}{
		{
			name: "with name:en",
			res:  "Hehe",
			in:   nameEn,
		},
		{
			name: "only name",
			res:  "Haha",
			in:   name,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			res := tt.in.Name()
			if tt.res != res {
				t.Fatalf("Expected: %s, got: %s", tt.res, res)
			}
		})
	}
}
