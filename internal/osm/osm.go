package osm

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"github.com/jftuga/geodist"
	"gitlab.gnome.org/daenney/castellanus/internal/geo"
	ihttp "gitlab.gnome.org/daenney/castellanus/internal/http"
	"gitlab.gnome.org/daenney/castellanus/internal/libgweather"
)

type Info struct {
	Name        string
	Coordinates geo.Coordinates
	Population  *uint64
	ICAO        *string
}

const BaseURL = "https://nominatim.openstreetmap.org"

var (
	ErrorNoMatch      = errors.New("query did not return a match")
	ErrorNoPopulation = errors.New("population data unavailable")
)

func GetCity(baseURL string, cl ihttp.Client, e libgweather.Entry) (Info, error) {
	base, err := url.Parse(baseURL + "/search")
	if err != nil {
		return Info{}, err
	}

	params := url.Values{}
	params.Add("format", "geocodejson")
	params.Add("limit", "2")
	params.Add("addressdetails", "1")
	params.Add("namedetails", "1")
	params.Add("accept-language", "en")
	params.Add("city", e.Name)
	if e.State != nil {
		params.Add("state", *e.State)
	}
	params.Add("country", e.Country)
	params.Add("countrycodes", strings.ToLower(e.CountryCode))

	base.RawQuery = params.Encode()

	data := resp{}
	err = ihttp.GetJSON(base.String(), cl, &data)
	if err != nil {
		return Info{}, err
	}

	if len(data.Features) == 0 && e.State != nil {
		// Lets try again but without the state, sometimes that messes it up
		params.Del("state")
		base.RawQuery = params.Encode()
		err = ihttp.GetJSON(base.String(), cl, &data)
		if err != nil {
			return Info{}, err
		}
	}

	if len(data.Features) == 0 {
		return Info{}, fmt.Errorf("no result for: %s %w", e.String(), ErrorNoMatch)
	}

	var ft feature

	if len(data.Features) == 1 {
		ft = data.Features[0]
	} else {
		ft = data.Features[0]
		if ft.Properties.Geocoding.OSMKey != "place" && data.Features[1].Properties.Geocoding.OSMKey == "place" &&
			data.Features[1].Properties.Geocoding.Type == "city" {
			ft = data.Features[1]
		}
	}

	gi := Info{
		Name: ft.Name(e.Name),
		Coordinates: geo.Coordinates{
			Coord: geodist.Coord{
				Lat: ft.Geometry.Coordinates[1],
				Lon: ft.Geometry.Coordinates[0],
			},
		},
	}

	pop, err := getPopulation(
		baseURL,
		cl,
		ft.Properties.Geocoding.OSMType,
		strconv.FormatUint(ft.Properties.Geocoding.OSMID, 10),
		ft.Properties.Geocoding.OSMKey,
	)
	if err == nil {
		gi.Population = &pop
	}
	if errors.Is(err, ErrorNoPopulation) {
		err = nil
	}

	return gi, err
}

type resp struct {
	Features []feature `json:"features"`
}

type feature struct {
	Type       string `json:"type"`
	Properties struct {
		Geocoding struct {
			OSMID   uint64 `json:"osm_id"`
			OSMType string `json:"osm_type"`
			OSMKey  string `json:"osm_key"`
			Type    string `json:"type"`
			Name    string `json:"name"`
			City    string `json:"city"`
		} `json:"geocoding"`
	} `json:"properties"`
	Geometry struct {
		Coordinates []float64 `json:"coordinates"`
	} `json:"geometry"`
}

// Name tries to see if there's a match in either the Name or the City attribute
func (f feature) Name(search string) string {
	name := f.Properties.Geocoding.Name
	if strings.EqualFold(search, name) {
		return name
	}

	city := f.Properties.Geocoding.City
	if strings.EqualFold(search, city) {
		return city
	}

	return name
}

type detailResp struct {
	Extratags struct {
		Population *string
	}
}

func (d *detailResp) UnmarshalJSON(data []byte) error {
	// Deal with the fact that Nominatim documents Extratags as a dictionary, but
	// actually gives you an array when it's empty
	type nonesense struct {
		Extratags []interface{} `json:"extratags"`
	}
	var n nonesense
	if err := json.Unmarshal(data, &n); err == nil {
		return nil
	}

	type resp struct {
		Extratags struct {
			Population *string `json:"population"`
		} `json:"extratags"`
	}

	var r resp
	if err := json.Unmarshal(data, &r); err != nil {
		return err
	}

	if r.Extratags.Population != nil {
		d.Extratags.Population = r.Extratags.Population
	}
	return nil
}

func getPopulation(baseURL string, cl ihttp.Client, rel, id, class string) (uint64, error) {
	base, err := url.Parse(baseURL + "/details")
	if err != nil {
		return 0, err
	}

	params := url.Values{}
	params.Add("format", "json")
	params.Add("osmtype", strings.ToUpper(string(rel[0])))
	params.Add("osmid", id)
	params.Add("class", class)
	params.Add("linkedplaces", "0")

	base.RawQuery = params.Encode()

	data := detailResp{}
	err = ihttp.GetJSON(base.String(), cl, &data)
	if err != nil {
		return 0, err
	}

	if data.Extratags.Population == nil {
		return 0, ErrorNoPopulation
	}

	i, err := strconv.ParseUint(*data.Extratags.Population, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("could not parse population as number: %v %w", err, ErrorNoPopulation)
	}

	return i, nil
}

type lookupResp struct {
	Lat         float64 `json:"lat,string"`
	Lon         float64 `json:"lon,string"`
	Class       string  `json:"class"`
	Type        string  `json:"type"`
	Namedetails struct {
		ICAO   string `json:"icao"`
		Name   string `json:"name"`
		NameEn string `json:"name:en"`
	} `json:"namedetails"`
}

func (d lookupResp) Name() string {
	if d.Namedetails.NameEn != "" {
		return d.Namedetails.NameEn
	}
	return d.Namedetails.Name
}

func (d lookupResp) Code() string {
	return d.Namedetails.ICAO
}

func (d lookupResp) Coordinates() geo.Coordinates {
	return geo.NewCoordinates(d.Lat, d.Lon)
}

func LookupObject(baseURL string, cl ihttp.Client, ids ...string) ([]Info, error) {
	base, err := url.Parse(baseURL + "/lookup")
	if err != nil {
		return nil, err
	}

	params := url.Values{}
	params.Add("format", "json")
	params.Add("extratags", "0")
	params.Add("addressdetails", "0")
	params.Add("namedetails", "1")
	params.Add("osm_ids", strings.Join(ids, ","))
	params.Add("accept-language", "en")

	base.RawQuery = params.Encode()

	data := []lookupResp{}
	err = ihttp.GetJSON(base.String(), cl, &data)
	if err != nil {
		return nil, err
	}

	if len(data) == 0 {
		return nil, ErrorNoMatch
	}

	res := make([]Info, 0, len(data))
	for _, d := range data {
		d := d
		r := Info{
			Name:        d.Name(),
			Coordinates: d.Coordinates(),
		}
		if d.Class == "aeroway" && d.Type == "aerodrome" {
			r.ICAO = &d.Namedetails.ICAO
		}
		res = append(res, r)
	}

	return res, nil
}
