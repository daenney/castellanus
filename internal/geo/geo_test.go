package geo

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"testing"

	"github.com/jftuga/geodist"
)

func TestDegreesToDecimal(t *testing.T) {
	tests := []struct {
		name string
		in   string
		res  float64
		err  bool
	}{
		{in: "51 53N", res: 51.883333},
		{in: "017 56E", res: 17.933333},
		{in: "09 52S", res: -9.866667},
		{in: "176 39W", res: -176.650000},
		{in: "51053N", res: InvalidCoordinate, err: true},
		{in: "BA NAS", res: InvalidCoordinate, err: true},
		{in: "", res: InvalidCoordinate, err: true, name: "empty string"},
	}

	for _, tt := range tests {
		tt := tt
		name := tt.in
		if tt.name != "" {
			name = tt.name
		}
		t.Run(name, func(t *testing.T) {
			t.Parallel()
			res, err := degreesToDecimal(tt.in)
			if err != nil && !tt.err {
				t.Fatalf("Did not expect an error, but got: '%s'", err)
			}
			if err != nil && tt.err && res != InvalidCoordinate {
				t.Fatalf("Expected InvalidCoordinate to be returned on error, got: '%f'", res)
			}
			if res != tt.res {
				t.Fatalf("Expected: '%f', got: '%f'", tt.res, res)
			}
		})
	}
}

func TestCoordinatesFromDegrees(t *testing.T) {
	tests := []struct {
		lat, lon string
		res      Coordinates
		err      bool
	}{
		{lat: "51 53N", lon: "176 39W", res: Coordinates{Coord: geodist.Coord{Lat: 51.883333, Lon: -176.65}}},
		{lat: "59 21N", lon: "017 56E", res: Coordinates{Coord: geodist.Coord{Lat: 59.35, Lon: 17.933333}}},
		{lat: "33 57S", lon: "151 10E", res: Coordinates{Coord: geodist.Coord{Lat: -33.95, Lon: 151.166667}}},
		{lat: "09 52S", lon: "056 06W", res: Coordinates{Coord: geodist.Coord{Lat: -9.866667, Lon: -56.1}}},
		{lat: "BA NAN", lon: "056 06W", res: Coordinates{Coord: geodist.Coord{Lat: InvalidCoordinate, Lon: InvalidCoordinate}}, err: true},
		{lat: "09 52S", lon: "BA NAW", res: Coordinates{Coord: geodist.Coord{Lat: -9.866667, Lon: InvalidCoordinate}}, err: true},
		{lat: "09 5SS", lon: "BA NAW", res: Coordinates{Coord: geodist.Coord{Lat: InvalidCoordinate, Lon: InvalidCoordinate}}, err: true},
		{lat: "51053N", lon: "176139W", res: Coordinates{Coord: geodist.Coord{Lat: InvalidCoordinate, Lon: InvalidCoordinate}}, err: true},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(fmt.Sprintf("%s %s", tt.lat, tt.lon), func(t *testing.T) {
			t.Parallel()
			coord, err := CoordinatesFromDegrees(tt.lat, tt.lon)
			if err != nil && !tt.err {
				t.Fatalf("Did not expect an error, but got: '%s'", err)
			}
			if err != nil && tt.err && coord.Lat != tt.res.Lat {
				t.Fatalf("Expected InvalidCoordinate to be returned on latitude error, got: '%f'", coord.Lat)
			}
			if err != nil && tt.err && coord.Lon != tt.res.Lon {
				t.Fatalf("Expected InvalidCoordinate to be returned on longitude error, got: '%f'", coord.Lon)
			}
			if coord.Lat != tt.res.Lat {
				t.Fatalf("Expected latitude: '%f', got: '%f'", tt.res.Lat, coord.Lat)
			}
			if coord.Lon != tt.res.Lon {
				t.Fatalf("Expected longitude: '%f', got: '%f'", tt.res.Lon, coord.Lon)
			}
		})
	}
}

func TestCoordinatesUnmarshalXML(t *testing.T) {
	tests := []struct {
		in  string
		out Coordinates
		err bool
	}{
		{
			in:  "<coord>1.0 1.0</coord>",
			out: Coordinates{geodist.Coord{Lat: 1.0, Lon: 1.0}},
			err: false,
		},
		{
			in:  "<coord>1.0</coord>",
			err: true,
		},
		{
			in:  "<coord>1.a 1.0</coord>",
			err: true,
		},
		{
			in:  "<coord>1.0 a</coord>",
			err: true,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.in, func(t *testing.T) {
			t.Parallel()
			d := xml.NewDecoder(bytes.NewBufferString(tt.in))
			res := Coordinates{}
			err := d.Decode(&res)
			if err != nil && !tt.err {
				t.Fatalf("Did not expect an error, got; %v", err)
			}
			if err == nil && tt.err {
				t.Fatalf("Expected an error, got nil")
			}
			if err == nil && !tt.err {
				if res.Coord.Lat != tt.out.Lat {
					t.Fatalf("Expected latitude: '%f', got: '%f'", tt.out.Lat, res.Coord.Lat)
				}
				if res.Coord.Lon != tt.out.Lon {
					t.Fatalf("Expected longitude: '%f', got: '%f'", tt.out.Lon, res.Coord.Lon)
				}
			}
		})
	}
}

func TestNewCoordinates(t *testing.T) {
	cc := NewCoordinates(1.0, 2.0)
	if cc.Lat != 1.0 {
		t.Fatalf("Expected latitude of 1.0, got: %f", cc.Lat)
	}
	if cc.Lon != 2.0 {
		t.Fatalf("Expected longitude of 2.0, got: %f", cc.Lon)
	}
}
