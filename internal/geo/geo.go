package geo

import (
	"encoding/xml"
	"fmt"
	"math"
	"strconv"
	"strings"

	"github.com/jftuga/geodist"
)

const (
	// InvalidCoordinate is a sentinel value that's returned together with an
	// error when parsing a degrees string fails. Valid latitude and longitude
	// only range from -90 to 90 and -180 to 180
	InvalidCoordinate = -200.0
)

type Coordinates struct {
	geodist.Coord
}

func NewCoordinates(lat, lon float64) Coordinates {
	return Coordinates{
		Coord: geodist.Coord{Lat: lat, Lon: lon},
	}
}

func (c Coordinates) String() string {
	return fmt.Sprintf("%.6f %.6f", c.Lat, c.Lon)
}

func (c *Coordinates) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var v string
	d.DecodeElement(&v, &start)
	coordinates := strings.Split(v, " ")

	if len(coordinates) != 2 {
		return fmt.Errorf("expected a pair of coordinates, got: %s", v)
	}

	lat, err := strconv.ParseFloat(coordinates[0], 64)
	if err != nil {
		return err
	}

	lon, err := strconv.ParseFloat(coordinates[1], 64)
	if err != nil {
		return err
	}

	*c = Coordinates{
		Coord: geodist.Coord{Lat: lat, Lon: lon},
	}

	return nil
}

func (c Coordinates) Distance(o Coordinates) float64 {
	_, km := geodist.HaversineDistance(c.Coord, o.Coord)

	return km
}

// CoordinatesFromDegrees take in a latitude and longitude string in
// the form of XXX YY(N|E|S|W) and returns a Coordinates holding the
// latitude and longitude as float64's.
func CoordinatesFromDegrees(lat, lon string) (Coordinates, error) {
	latf, err := degreesToDecimal(lat)
	if err != nil {
		return Coordinates{
			Coord: geodist.Coord{Lat: InvalidCoordinate, Lon: InvalidCoordinate},
		}, err
	}
	lonf, err := degreesToDecimal(lon)

	return Coordinates{Coord: geodist.Coord{Lat: latf, Lon: lonf}}, err
}

func degreesToDecimal(deg string) (float64, error) {
	if deg == "" {
		return InvalidCoordinate, fmt.Errorf("invalid input: empty string")
	}
	degs := strings.Split(deg[:len(deg)-1], " ")
	if len(degs) != 2 {
		return InvalidCoordinate, fmt.Errorf("invalid format: %s", deg)
	}
	degrees := degs[0]
	minutes := degs[1]

	degreesf, err := strconv.ParseFloat(degrees, 64)
	if err != nil {
		return InvalidCoordinate, err
	}
	minutesf, err := strconv.ParseFloat(minutes, 64)
	if err != nil {
		return InvalidCoordinate, err
	}

	decimals := math.Pow(10, 6.0)
	degreesf = math.Round(degreesf*decimals) / decimals
	minutesf = math.Round((minutesf/60)*decimals) / decimals
	res := degreesf + minutesf

	dir := string(deg[len(deg)-1])
	if dir == "W" || dir == "S" {
		return -1 * res, nil
	}

	return res, nil
}
